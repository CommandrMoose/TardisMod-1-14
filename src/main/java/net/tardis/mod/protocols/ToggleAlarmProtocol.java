package net.tardis.mod.protocols;

import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.tileentities.ConsoleTile;

public class ToggleAlarmProtocol extends Protocol {

	@Override
	public void call(World world, ConsoleTile console) {
		if(!world.isRemote) 
			console.getInteriorManager().setAlarmOn(!console.getInteriorManager().isAlarmOn());
		else Tardis.proxy.openGUI(Constants.Gui.NONE, new GuiContext());
	}

	@Override
	public String getDisplayName() {
		return "Toggle Alarm";
	}

	@Override
	public String getSubmenu() {
		return "security";
	}

}

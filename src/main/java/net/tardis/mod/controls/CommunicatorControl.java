package net.tardis.mod.controls;

import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SoundSource;
import net.minecraft.client.particle.ParticleManager;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.item.MusicDiscItem;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.experimental.advancement.TTriggers;
import net.tardis.mod.items.ScrapbookItem;
import net.tardis.mod.misc.ITickable;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;

import java.util.Random;


public class CommunicatorControl extends BaseControl implements ITickable{
	
	private static String TRANS = "status.tardis.communicator.located";
	private static final String NOWPLAYING = "status.tardis.communicator.nowplaying";
	private static final String HIDDEN = "status.tardis.communicator.hidden";
	public boolean musicParticles = false;

	public CommunicatorControl(ConsoleTile console) {
		super(console);
		if(this.getConsole() != null)
			this.getConsole().registerTicker(this);
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(0.3F, 0.3F);
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.1625F, 0.1625F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.3125F, 0.3125F);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return EntitySize.flexible(0.375F, 0.375F);

		return EntitySize.flexible(0.4F, 0.4F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		/*if(!console.getWorld().isRemote) {
			if(!console.getDistressSignals().isEmpty()) {
				BlockPos pos = console.getDistressSignals().get(0);
				console.getDistressSignals().remove(0);
				player.sendMessage(new TranslationTextComponent(TRANS, Helper.formatBlockPos(pos)));
			}
			this.setAnimationTicks(20);
		}*/


		if (!console.getWorld().isRemote) {
		// Todo: Make this fucker a packet or something hahahahahahahahahahahaha.
			Hand handIn = player.getActiveHand();
			Item itemInHand = player.getHeldItem(handIn).getItem();
			this.musicParticles = false;

			Tardis.proxy.stopSoundEvent(null, SoundCategory.RECORDS);

			if (itemInHand instanceof MusicDiscItem) {
				this.musicParticles = true;
				player.sendStatusMessage(new TranslationTextComponent(NOWPLAYING, ((MusicDiscItem) itemInHand).getRecordDescription()), true);
				((MusicDiscItem) itemInHand).getRecordDescription().getString();
				console.getWorld().playSound(null, console.getPos(), ((MusicDiscItem) itemInHand).getSound(), SoundCategory.RECORDS, 1000 , 1);
			}

			this.setAnimationTicks(20);

		} else {
			if(console.getWorld().isRemote && !console.getDistressSignals().isEmpty())
				Tardis.proxy.openGUI(Constants.Gui.COMMUNICATOR, null);
		}
		return true;
	}

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(6 / 16.0, 12 / 16.0, -4 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(-0.5577870538580397, 0.33124999701976776, -0.6428223124704939);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(0.001729726741766413, 0.8125, 0.7897286577965876);
		}
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(0.23559568057373126, 0.59375, 0.713925883398866);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return new Vec3d(-0.0908606167517797, 0.5937, -0.8449081117709261);

		return new Vec3d(0, 8 / 16.0, -6 / 16.0);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return null;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return console instanceof NemoConsoleTile ? SoundEvents.BLOCK_BELL_USE : TSounds.COMMUNICATOR_STEAM;
	}


	@Override
	public void tick(ConsoleTile console) {

		if (console == null || console.getWorld() == null)
			return;

		if (!console.getWorld().isRemote && !console.getDistressSignals().isEmpty() && console.getWorld().getGameTime() % 100 == 0) {
			console.getWorld().playSound(null, console.getPos(), TSounds.COMMUNICATOR_BEEP, SoundCategory.BLOCKS, 1F, 1F);
		}

		// Check if the world is the remote/client
		if (console.getWorld().isRemote) {
			if (musicParticles) {
				animateTick(console.getWorld());
			}
		}
	}



	@OnlyIn(Dist.CLIENT)
	public void animateTick(World worldIn) {
		Random random = new Random();
		int range = 10;
			if(random.nextDouble() < 0.90) {
				worldIn.addParticle(ParticleTypes.NOTE, getConsole().getPos().getX() + random.nextInt(range) - (range/2), getConsole().getPos().getY() + random.nextInt(range) - (range/2), getConsole().getPos().getZ() + random.nextInt(range) - (range/2), 0, 0, 0);
				}
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putBoolean("playing_music", this.musicParticles);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.musicParticles = nbt.getBoolean("playing_music");
	}
}

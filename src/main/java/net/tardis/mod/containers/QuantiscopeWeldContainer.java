package net.tardis.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.tileentities.QuantiscopeTile;

public class QuantiscopeWeldContainer extends Container{

	private QuantiscopeTile tile;
	
	protected QuantiscopeWeldContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	
	public QuantiscopeWeldContainer(int id, PlayerInventory inv, PacketBuffer buf) {
		super(TContainers.QUANTISCOPE_WELD, id);
		this.init(inv, (QuantiscopeTile)inv.player.world.getTileEntity(buf.readBlockPos()));
	}
	
	public QuantiscopeWeldContainer(int id, PlayerInventory inv, QuantiscopeTile tile) {
		super(TContainers.QUANTISCOPE_WELD, id);
		this.init(inv, tile);
	}
	
	public void init(PlayerInventory inv, QuantiscopeTile quantiscope) {
		
		tile = quantiscope;
		
		//Parts
		this.addSlot(new SlotItemHandler(quantiscope, 0, 22, 1));
		this.addSlot(new SlotItemHandler(quantiscope, 1, 48, 1));
		this.addSlot(new SlotItemHandler(quantiscope, 2, 22, 27));
		this.addSlot(new SlotItemHandler(quantiscope, 3, 22, 52));
		this.addSlot(new SlotItemHandler(quantiscope, 4, 48, 52));
		
		//Thing to repair
		this.addSlot(new SlotItemHandler(quantiscope, 5, 60, 27));
		//Repaired Item
		this.addSlot(new SlotItemHandler(quantiscope, 6, 134, 27));
		
		//Player Inv
		for(int l = 0; l < 3; ++l) {
	         for(int j1 = 0; j1 < 9; ++j1) {
	            this.addSlot(new Slot(inv, j1 + l * 9 + 9, 8 + j1 * 18, 103 + l * 18 + 3));
	         }
	      }

	      for(int i1 = 0; i1 < 9; ++i1) {
	         this.addSlot(new Slot(inv, i1, 8 + i1 * 18, 161 + 3));
	      }
	     
	}
	
	public QuantiscopeTile getQuantiscope() {
		return this.tile;
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	}
	
	@Override
	public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
		Slot slot = this.getSlot(index);
		if(slot.inventory instanceof PlayerInventory) {
			
		}
		return ItemStack.EMPTY;
	}

}

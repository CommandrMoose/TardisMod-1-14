package net.tardis.mod.containers.slot;

import java.util.function.Predicate;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class FilteredSlot extends Slot{

	private Predicate<Item> filter;
	
	public FilteredSlot(IInventory inventoryIn, int index, int xPosition, int yPosition, Predicate<Item> filter) {
		super(inventoryIn, index, xPosition, yPosition);
		this.filter = filter;
	}

	@Override
	public boolean isItemValid(ItemStack stack) {
		return filter.test(stack.getItem());
	}

}

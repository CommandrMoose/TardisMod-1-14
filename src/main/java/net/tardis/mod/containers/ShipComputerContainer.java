package net.tardis.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.tileentities.ShipComputerTile;

public class ShipComputerContainer extends BaseContainer{

	public ShipComputerContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	
	//Client
	public ShipComputerContainer(int id, PlayerInventory inv, PacketBuffer buf) {
		this(TContainers.SHIP_COMPUTER, id);
		init(inv, (ShipComputerTile)inv.player.world.getTileEntity(buf.readBlockPos()));
	}
	
	//Server
	public ShipComputerContainer(int id, PlayerInventory player, IItemHandlerModifiable tile) {
		this(TContainers.SHIP_COMPUTER, id);
		init(player, tile);
	}
	
	public void init(PlayerInventory player, IItemHandlerModifiable inv) {
		
		for(int i = 0; i < inv.getSlots(); ++i) {
			this.addSlot(new SlotItemHandler(inv, i, 8 + (i % 9) * 18, 18 + (i / 9) * 18));
		}
		
		Helper.addPlayerInvContainer(this, player, 0, 0);
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	
	}

}

package net.tardis.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.tardis.mod.containers.slot.FilteredSlot;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.tileentities.ReclamationTile;

public class ReclamationContainer extends BaseContainer{

	public ReclamationContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	
	//Client	
	public ReclamationContainer(int id, PlayerInventory inv, PacketBuffer buf) {
		super(TContainers.RECLAMATION_UNIT, id);
		
		TileEntity te = inv.player.world.getTileEntity(buf.readBlockPos());
		if(te instanceof ReclamationTile)
			init(inv, ((ReclamationTile)te));
	}
	
	//Server
	public ReclamationContainer(int id, PlayerInventory inv, ReclamationTile tileInv) {
		super(TContainers.RECLAMATION_UNIT, id);
		init(inv, tileInv);
	}
	
	//Common
	public void init(PlayerInventory player, ReclamationTile tile) {
		
		for(int i = 0; i < 54; ++i) {
			this.addSlot(new FilteredSlot(tile, i, 8 + (i % 9) * 18, -10 + (i / 9) * 18, stack -> false));
		}
		
		Helper.addPlayerInvContainer(this, player, 0, 26);
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	}
	

}

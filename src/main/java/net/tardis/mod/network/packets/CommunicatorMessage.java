package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.SpaceTimeCoord;

public class CommunicatorMessage {

	boolean accepted;
	
	public CommunicatorMessage(boolean accepted) {
		this.accepted = accepted;
	}
	
	public static void encode(CommunicatorMessage mes, PacketBuffer buf) {
		buf.writeBoolean(mes.accepted);
	}
	
	public static CommunicatorMessage decode(PacketBuffer buf) {
		return new CommunicatorMessage(buf.readBoolean());
	}
	
	public static void handle(CommunicatorMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			TardisHelper.getConsoleInWorld(cont.get().getSender().world).ifPresent(tile -> {
				if(!tile.getDistressSignals().isEmpty()) {
					if(mes.accepted) {
						SpaceTimeCoord pos = tile.getDistressSignals().get(0);
						tile.setDestination(DimensionType.byName(pos.getDimType()), pos.getPos());
					}
					tile.getDistressSignals().remove(0);
					tile.updateClient();
				}
			});
				
		});
		cont.get().setPacketHandled(true);
	}
}

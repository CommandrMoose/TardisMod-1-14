package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.TardisHelper;

public class ChangeExtVarMessage {

	private int index = 0;
	
	public ChangeExtVarMessage(int index) {
		this.index = index;
	}
	
	public static void encode(ChangeExtVarMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.index);
	}
	
	public static ChangeExtVarMessage decode(PacketBuffer buf) {
		return new ChangeExtVarMessage(buf.readInt());
	}
	
	public static void handle(ChangeExtVarMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			TardisHelper.getConsoleInWorld(context.get().getSender().getServerWorld()).ifPresent(tile -> {
				tile.getExteriorManager().setExteriorVariant(mes.index);
				tile.updateClient();
			});
		});
		context.get().setPacketHandled(true);
	}
}

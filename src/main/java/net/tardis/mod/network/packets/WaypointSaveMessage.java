package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.tileentities.ConsoleTile;

public class WaypointSaveMessage {

	private String name;
	
	public WaypointSaveMessage(String name) {
		this.name = name;
	}
	
	public static void encode(WaypointSaveMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.name.length());
		buf.writeString(mes.name, mes.name.length());
	}
	
	public static WaypointSaveMessage decode(PacketBuffer buf) {
		int len = buf.readInt();
		return new WaypointSaveMessage(buf.readString(len));
	}
	
	public static void handle(WaypointSaveMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			TileEntity te = cont.get().getSender().world.getTileEntity(TardisHelper.TARDIS_POS);
			if(te instanceof ConsoleTile) {
				ConsoleTile console = ((ConsoleTile)te);
				//If it has an empty one
				if(console.getWaypoints().contains(SpaceTimeCoord.UNIVERAL_CENTER)) {
					SpaceTimeCoord coord = new SpaceTimeCoord(console.getDimension(), console.getLocation(), console.getExteriorDirection());
					coord.setName(mes.name);
					for(int i = 0; i < console.getWaypoints().size(); ++i) {
						if(console.getWaypoints().get(i).equals(SpaceTimeCoord.UNIVERAL_CENTER)) {
							console.getWaypoints().set(i, coord);
							console.updateClient();
							break;
						}
					}
				}
			}
		});
		cont.get().setPacketHandled(true);
	}
}

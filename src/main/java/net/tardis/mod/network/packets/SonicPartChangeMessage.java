package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.items.SonicItem;
import net.tardis.mod.items.sonicparts.SonicBasePart;
import net.tardis.mod.items.sonicparts.SonicBasePart.SonicComponentTypes;
import net.tardis.mod.sonic.ISonicPart.SonicPart;

public class SonicPartChangeMessage {
	
	ItemStack stack;
	BlockPos pos;
	int slot;
	
	public SonicPartChangeMessage(ItemStack stack, int slot, BlockPos pos) {
		this.stack = stack;
		this.pos = pos;
		this.slot = slot;
	}
	
	public static void encode(SonicPartChangeMessage mes, PacketBuffer buf) {
		buf.writeItemStack(mes.stack);
		buf.writeInt(mes.slot);
		buf.writeBlockPos(mes.pos);
	}
	
	public static SonicPartChangeMessage decode(PacketBuffer buf) {
		return new SonicPartChangeMessage(buf.readItemStack(), buf.readInt(), buf.readBlockPos());
	}
	
	public static void handle(SonicPartChangeMessage mes, Supplier<NetworkEvent.Context> con) {
		con.get().enqueueWork(() -> {
			TileEntity te = con.get().getSender().world.getTileEntity(mes.pos);
			if(te instanceof IInventory) {
				IInventory workbench = (IInventory)te;
				ItemStack sonic = workbench.getStackInSlot(0);
				if(sonic.getItem() instanceof SonicItem) {
					sonic.getCapability(Capabilities.SONIC_CAPABILITY).ifPresent(cap -> {
						int type = SonicBasePart.getType(mes.stack);
						cap.setSonicPart(SonicComponentTypes.getFromID(type), SonicPart.getFromItem(mes.stack.getItem()));
						SonicItem.syncCapability(sonic);
					});
				}
			}
		});
		con.get().setPacketHandled(true);
	}

}

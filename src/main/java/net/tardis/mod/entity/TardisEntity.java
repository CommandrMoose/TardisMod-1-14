package net.tardis.mod.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.exterior.IExterior;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;

public class TardisEntity extends Entity{

	public static final DataParameter<String> EXTERIOR = EntityDataManager.createKey(TardisEntity.class, DataSerializers.STRING);
	private ConsoleTile console;
	private int sneakTicks = 0;
    private IExterior exterior;
	
	public TardisEntity(EntityType<?> entityTypeIn, World worldIn) {
		super(entityTypeIn, worldIn);
	}
	
	public TardisEntity(World worldIn) {
		super(TEntities.TARDIS, worldIn);
	}

	@Override
	protected void registerData() {
		this.getDataManager().register(EXTERIOR, ExteriorRegistry.TRUNK.getRegistryName().toString());
	}

	@Override
	protected void readAdditional(CompoundNBT compound) {}

	@Override
	protected void writeAdditional(CompoundNBT compound) {}

    @Override
    public void onRemovedFromWorld() {
        super.onRemovedFromWorld();
        if (!world.isRemote)
            land();
    }

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	public void tick() {
		super.tick();
		this.move(MoverType.SELF, this.getMotion());
		if(!this.onGround && !this.hasNoGravity() && this.getRidingEntity() != null)
			this.setMotion(this.getMotion().add(0, -0.04, 0));
	}
	
	@Override
	public void updateRidden() {
		super.updateRidden();
		/*this.prevPosX = this.posX;
		this.prevPosY = this.posY;
		this.prevPosZ = this.posZ;
		
		this.posX = this.getRidingEntity().posX;
		this.posY = this.getRidingEntity().posY;
		this.posZ = this.getRidingEntity().posZ;*/

        if (this.onGround) {

            if (this.getRidingEntity().isSneaking())
                ++sneakTicks;
            else this.sneakTicks = 0;
            if (sneakTicks > 30)
                land();
        }
        else this.sneakTicks = 0;

        if (world.getGameTime() % 200 == 0) {
            this.exterior = ExteriorRegistry.getExterior(new ResourceLocation(this.getDataManager().get(EXTERIOR)));
        }
	}
	
	@Override
	public boolean processInitialInteract(PlayerEntity player, Hand hand) {
		if(!world.isRemote) {
			if(console == null)
				console = (ConsoleTile)world.getTileEntity(TardisHelper.TARDIS_POS);
			return true;
		}
		this.startRiding(player, true);
		return super.processInitialInteract(player, hand);
	}

	public void land() {
        if (!world.isRemote && console != null) {
			console.getExterior().place(console, world.getDimension().getType(), this.getPosition());
			console.setLocation(world.getDimension().getType(), this.getPosition());
        }
        remove();
	}

	@Override
	public boolean canBeCollidedWith() {
		return true;
	}

	@Override
	public boolean canBePushed() {
		return true;
	}

    public IExterior getExterior() {
        if (exterior == null) {
            this.exterior = ExteriorRegistry.getExterior(new ResourceLocation(this.getDataManager().get(EXTERIOR)));
        }
        return this.exterior;
    }

    public void setConsole(ConsoleTile tile) {
        this.console = tile;
        if (!world.isRemote)
            this.getDataManager().set(EXTERIOR, DimensionType.getKey(tile.getWorld().dimension.getType()).toString());
    }

}

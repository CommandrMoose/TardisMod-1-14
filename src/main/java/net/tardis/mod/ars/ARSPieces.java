package net.tardis.mod.ars;

import java.io.IOException;
import java.io.InputStreamReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import net.minecraft.resources.IResource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ARSPiece.CorridorType;
import net.tardis.mod.registries.TardisRegistries;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class ARSPieces {
	
	@SubscribeEvent
	public static void register(FMLCommonSetupEvent event) {
		register("straight", new ARSPiece("straight", new BlockPos(3, 2, 21), CorridorType.CORRIDOR_3x3));
		register("t3", new ARSPiece("t_3", new BlockPos(6, 2, 9), CorridorType.CORRIDOR_3x3));
		register("fourway", new ARSPiece("fourway", new BlockPos(9, 2, 19), CorridorType.CORRIDOR_3x3));
		register("corner_right", new ARSPiece("corner_right", new BlockPos(3, 2, 10), CorridorType.CORRIDOR_3x3));
		register("corner_left", new ARSPiece("corner_left", new BlockPos(6, 2, 10), CorridorType.CORRIDOR_3x3));
		
		//Rooms
		register("small", new ARSPiece("small", new BlockPos(10, 2, 26), CorridorType.CORRIDOR_3x3));
		register("library", new ARSPiece("library", new BlockPos(13, 2, 29), CorridorType.CORRIDOR_3x4));
		register("lab", new ARSPiece("lab", new BlockPos(10, 2, 18), CorridorType.CORRIDOR_3x3));

	}
	
	public static ARSPiece register(String name, ARSPiece piece) {
		TardisRegistries.ARS_PIECES.register(name, piece);
		return piece;
	}
	
	@SubscribeEvent
	public static void read(FMLServerStartingEvent event) {
		try {
			IResource res = event.getServer().getResourceManager().getResource(new ResourceLocation(Tardis.MODID, "ars/parts.json"));
			
			JsonArray array = new JsonParser().parse(new InputStreamReader(res.getInputStream())).getAsJsonArray();
			for(JsonElement ele : array) {
				if(ele.isJsonObject()) {
					
				}
			}
		}
		catch(IOException e) {}
	}
}

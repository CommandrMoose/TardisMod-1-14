package net.tardis.mod.ars;

import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.registries.IRegisterable;

public class ARSPiece implements IRegisterable<ARSPiece>{
	
	private ResourceLocation name;
	
	private ResourceLocation struct;
	private BlockPos offset;
	private CorridorType type;
	
	/**
	 * 
	 * @param loc - Registry name
	 * @param offset - the offset from the ARS block to spawn this
	 * @param type - Not used
	 */
	public ARSPiece(ResourceLocation loc, BlockPos offset, CorridorType type) {
		this.struct = loc;
		this.offset = offset;
		this.type = type;
	}
	
	public ARSPiece(String loc, BlockPos offset, CorridorType type) {
		this(new ResourceLocation(Tardis.MODID, "tardis/structures/ars/" + loc), offset, type);
	}
	
	public boolean spawn(ServerWorld world, BlockPos pos, PlayerEntity player, Direction dir) {
		Template temp = world.getStructureTemplateManager().getTemplate(struct);
		
		BlockPos offset = Helper.rotateBlockPos(this.offset, dir);
		
		if(temp != null) {
			
			BlockPos spawnStart = pos.subtract(offset);
			BlockPos endPos = spawnStart.add(Helper.rotateBlockPos(temp.getSize().north(2).west(), dir));
			for(BlockPos check : BlockPos.getAllInBoxMutable(spawnStart, endPos)) {
				if(!world.getBlockState(check).isAir()) {
					player.sendStatusMessage(Constants.Translations.CORRIDOR_BLOCKED, true);
					return false;
				}
			}
			
			for(int x = -1; x <= 1; ++x) {
				for(int y = -1; y < (type == CorridorType.CORRIDOR_3x3 ? 2 : 3); ++y) {
					if(dir == Direction.SOUTH || dir == Direction.NORTH)
						world.setBlockState(pos.add(x, y, 0), Blocks.AIR.getDefaultState());
					else world.setBlockState(pos.add(0, y, x), Blocks.AIR.getDefaultState());
				}
			}
			
			PlacementSettings set = new PlacementSettings().setIgnoreEntities(false).setRotation(Helper.getRotationFromDirection(dir));
			temp.addBlocksToWorld(world, spawnStart, set);
			return true;
			
		}
		else System.err.println("WARNING: Could not load structure " + this.struct);
		return false;
		
	}

	@Override
	public ARSPiece setRegistryName(ResourceLocation regName) {
		this.name = regName;
		return this;
	}

	@Override
	public ResourceLocation getRegistryName() {
		return this.name;
	}
	
	public TranslationTextComponent getTranslation() {
		return new TranslationTextComponent("ars.piece." + this.name.getNamespace() + "." + this.name.getPath());
	}
	
	public static enum CorridorType{
		CORRIDOR_3x3,
		CORRIDOR_3x4;
	}
}
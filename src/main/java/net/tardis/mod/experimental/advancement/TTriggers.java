package net.tardis.mod.experimental.advancement;

import net.minecraft.advancements.CriteriaTriggers;

/**
 * Created by Swirtzly
 * on 19/04/2020 @ 12:33
 */
//Now now, let's not get triggered
public class TTriggers {

    public static final TriggerBase OBTAINED = new TriggerBase("obtain_tardis");
    public static final TriggerBase TARDIS_MOD = new TriggerBase("tardis_mod");
    public static final TriggerBase NEW_WHO = new TriggerBase("new_who_unlock");

    public static void init() {
        CriteriaTriggers.register(OBTAINED);
        CriteriaTriggers.register(TARDIS_MOD);
        CriteriaTriggers.register(NEW_WHO);
    }

}

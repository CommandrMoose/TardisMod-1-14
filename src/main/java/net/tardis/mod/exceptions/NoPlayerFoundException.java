package net.tardis.mod.exceptions;

public class NoPlayerFoundException extends Exception {
    private String username;

    public NoPlayerFoundException(String username) {
        this.username = username;
    }

    @Override
    public String getMessage() {
        return "No player found for the username: " + username;
    }
}

package net.tardis.mod.texturevariants;

import net.tardis.mod.misc.TexVariant;

public class TextureVariants {

	public static final TexVariant[] TRUNK = {
			new TexVariant("trunk_base", "exterior.trunk.normal"),
			new TexVariant("trunk_dark", "exterior.trunk.dark")
	};
	
	public static final TexVariant[] FORTUNE = {
			new TexVariant("fortune", "exterior.fortune.normal"),
			new TexVariant("fortune_blue", "exterior.fortune.blue"),
			new TexVariant("fortune_red", "exterior.fortune.red")
	};
	
	public static final TexVariant[] STEAM = {
			new TexVariant("steampunk", "exterior.steam.normal"),
			new TexVariant("steam_blue", "exterior.steam.blue"),
			new TexVariant("steam_rust", "exterior.steam.rust")
	};

	public static final TexVariant[] MODERN_POLICE_BOX = {
			new TexVariant("modern_police_box", "exterior.modern_police_box.normal"),
			new TexVariant("modern_police_box_war", "exterior.modern_police_box.war"),
			new TexVariant("modern_police_box_pink", "exterior.modern_police_box.pink"),
			new TexVariant("modern_police_box_jodie", "exterior.modern_police_box.jodie")
	};
}

package net.tardis.mod.texturevariants;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.misc.TexVariant;

public class ConsoleTextureVariants {

	public static TexVariant[] NEMO = {
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/nemo.png"), "console.nemo.normal"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/nemo_ivory.png"), "console.nemo.ivory"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/nemo_wood.png"), "console.nemo.wood")
	};
	
	public static TexVariant[] STEAM = {
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/steam.png"), "console.steam.normal"),
			new TexVariant(new ResourceLocation(Tardis.MODID, "textures/consoles/steam_ironclad.png"), "console.steam.ironclad")
	};
}

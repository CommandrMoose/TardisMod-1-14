package net.tardis.mod.cap.items;

import java.util.UUID;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.Tardis;
import net.tardis.mod.controls.StabilizerControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.tileentities.ConsoleTile;

public class RemoteCapability implements IRemote{

	private ItemStack remote;
	private UUID owner = null;
	private ConsoleTile tile;

	//Client varaibles
	private int timeLeft = 0;
	private SpaceTimeCoord location = SpaceTimeCoord.UNIVERAL_CENTER;
	
	public RemoteCapability(ItemStack stack) {
		this.remote = stack;
	}
	
	@Override
	public SpaceTimeCoord getExteriorLocation() {
		return this.location;
	}

	@Override
	public int getTimeToArrival() {
		return this.timeLeft;
	}

	@Override
	public UUID getOwner() {
		return this.owner;
	}
	
	@Override
	public void setOwner(UUID id) {
		this.owner = id;	
	}

	@Override
	public void onClick(World world, PlayerEntity player, BlockPos pos) {
		if(!world.isRemote && owner != null) {
			if(tile == null || tile.isRemoved()) {
				DimensionType type = DimensionType.byName(new ResourceLocation(Tardis.MODID, owner.toString()));
				if(type != null) {
					ServerWorld inter = world.getServer().getWorld(type);
					if(inter != null) {
						TileEntity te = inter.getTileEntity(TardisHelper.TARDIS_POS);
						if(te instanceof ConsoleTile)
							this.tile = (ConsoleTile)te;
					}
				}
			}
			
			if(tile != null && !tile.isRemoved()) {
				tile.setDestination(player.dimension, pos.up());
				StabilizerControl stabilizer = tile.getControl(StabilizerControl.class);
				ThrottleControl throttle = tile.getControl(ThrottleControl.class);
				if(throttle != null)
					throttle.setAmount(1.0F);
				if (stabilizer != null)
					stabilizer.setStabilized(true);
				tile.takeoff();
			}
			
		}
	}
	
	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		if(owner != null)
			tag.putString("owner", this.owner.toString());
		tag.putInt("time_left", this.timeLeft);
		tag.put("loc", this.location.serialize());
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		if(nbt.contains("owner"))
			this.owner = UUID.fromString(nbt.getString("owner"));
		if(nbt.contains("loc"))
			this.location = SpaceTimeCoord.deserialize(nbt.getCompound("loc"));
		this.timeLeft = nbt.getInt("time_left");
	}
	
	private void update(PlayerEntity player, Hand hand) {
		
	}
	
	@Override
	public void setExteriorLocation(SpaceTimeCoord coord) {
		this.location = coord;
		
	}

	@Override
	public void setTimeToArrival(int time) {
		this.timeLeft = time;
		
	}

	@Override
	public void tick(World world, Entity ent) {
		if (this.tile != null && owner !=null) {
			this.setExteriorLocation(new SpaceTimeCoord(tile.getDestinationDimension(),tile.getDestination()));
			this.setTimeToArrival(tile.getTimeLeft());
//			this.setOwner(ent.getUniqueID());
		}
	}

	@Override
	public BlockPos getExteriorPos() {
		return this.location.getPos();
	}

	@Override
	public DimensionType getExteriorDim() {
		return DimensionType.byName(this.location.getDimType());
	}

}

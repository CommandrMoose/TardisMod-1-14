package net.tardis.mod.cap.entity;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.misc.SpaceTimeCoord;

public class PlayerDataCapability implements IPlayerData {

    private PlayerEntity player;
    private SpaceTimeCoord coord = SpaceTimeCoord.UNIVERAL_CENTER;
    private boolean usedTelepathics = false;
    private double displacement;

    public PlayerDataCapability(PlayerEntity ent) {
        this.player = ent;
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.put("coord", this.coord.serialize());
        tag.putBoolean("telepath", this.usedTelepathics);
        tag.putDouble("displacement", this.displacement);
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        if (nbt.contains("coord"))
            this.coord = SpaceTimeCoord.deserialize(nbt.getCompound("coord"));
        	this.usedTelepathics = nbt.getBoolean("telepath");
        	this.displacement = nbt.getDouble("displacement");

    }

    @Override
    public SpaceTimeCoord getDestination() {
        return coord;
    }

    @Override
    public void setDestination(SpaceTimeCoord coord) {
        this.coord = coord;
    }

    @Override
	public void setHasUsedTelepathics(boolean used) {
		this.usedTelepathics = used;
	}

	@Override
	public boolean hasUsedTelepathics() {
		return this.usedTelepathics;
	}
	
    @Override
    public void tick() {
        if (player instanceof ServerPlayerEntity) {
        	ServerPlayerEntity serverPlayer = (ServerPlayerEntity)player;
            if (serverPlayer.dimension != null && serverPlayer.dimension.getModType() == TDimensions.VORTEX) {
                if (serverPlayer.posY < 0) {
                    ServerWorld world = player.world.getServer().getWorld(DimensionType.byName(coord.getDimType()));
                    if (world != null) {
                	  if (world.getDimension().getType() == TDimensions.VORTEX_TYPE) {
                		  ServerWorld worldDest = player.world.getServer().getWorld(DimensionType.OVERWORLD);
                		  world = worldDest; //Set dimension to Overworld if they try to teleport whilst in the vortex
                      }
                    BlockPos pos = coord.getPos();
                    serverPlayer.fallDistance = 0;
                    serverPlayer.teleport(world, pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5, 0, 0);
                    }
                }
            }
        }
    }

	@Override
	public double getDisplacement() {
		return this.displacement;
	}

	@Override
	public void calcDisplacement(BlockPos start, BlockPos finish) {
		this.displacement = Math.sqrt(start.distanceSq(finish));
	}
}

package net.tardis.mod.tileentities;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;


public class ReclamationTile extends TileEntity implements IInventory, ITickableTileEntity{
	
	private List<ItemStack> inv = new ArrayList<ItemStack>();
	
	public ReclamationTile() {
		super(TTiles.RECLAMATION_UNIT);
	}

	@Override
	public void tick() {
		if(!world.isRemote && this.isEmpty())
			world.setBlockState(this.getPos(), Blocks.AIR.getDefaultState());
	}

	@Override
	public ItemStack getStackInSlot(int slot) {
		return inv.size() < slot ? inv.get(slot) : ItemStack.EMPTY;
	}

	@Override
	public void clear() {
		inv.clear();
		this.markDirty();
	}

	@Override
	public int getSizeInventory() {
		return inv.size();
	}

	@Override
	public boolean isEmpty() {
		return inv.isEmpty();
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		this.markDirty();
		return this.getStackInSlot(index).split(count);
	}

	@Override
	public ItemStack removeStackFromSlot(int index) {
		ItemStack stack = this.getStackInSlot(index);
		this.setInventorySlotContents(index, ItemStack.EMPTY);
		this.markDirty();
		return stack;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		if(this.inv.size() < index)
			this.inv.set(index, stack);
		else inv.add(stack);
	}

	@Override
	public boolean isUsableByPlayer(PlayerEntity player) {
		return true;
	}
	
	public void addItemStack(ItemStack stack) {
		this.markDirty();
		this.inv.add(stack);
	}
	
}

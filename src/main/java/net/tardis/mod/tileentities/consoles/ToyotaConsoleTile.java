package net.tardis.mod.tileentities.consoles;

import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.TTiles;

public class ToyotaConsoleTile extends ConsoleTile{

	public ToyotaConsoleTile() {
		super(TTiles.CONSOLE_TOYOTA);
	}

}

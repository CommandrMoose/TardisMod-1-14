package net.tardis.mod.tileentities.consoles;

import net.minecraft.tileentity.TileEntityType;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.TTiles;

public class ArtDecoConsoleTile extends ConsoleTile{

	public ArtDecoConsoleTile(TileEntityType<?> type) {
		super(type);
	}
	
	public ArtDecoConsoleTile() {
		super(TTiles.CONSOLE_ART_DECO);
	}

}

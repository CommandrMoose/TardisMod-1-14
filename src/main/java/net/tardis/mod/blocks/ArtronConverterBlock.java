package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;
import net.tardis.mod.properties.Prop;

public class ArtronConverterBlock extends TileBlock {

	public static BooleanProperty EMMIT = BooleanProperty.create("emmit");
	
	public ArtronConverterBlock() {
		super(Prop.Blocks.BASIC_TECH.get());
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		if(!worldIn.isRemote && handIn == player.getActiveHand()) {
			worldIn.setBlockState(pos, state.with(EMMIT, !state.get(EMMIT)));
		}
		return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
	}
	
	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		builder.add(EMMIT);
	}
}

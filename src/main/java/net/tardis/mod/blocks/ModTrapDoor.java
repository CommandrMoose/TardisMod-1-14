package net.tardis.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.block.TrapDoorBlock;
import net.minecraft.item.Item;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;

import java.util.function.Supplier;

public class ModTrapDoor extends TrapDoorBlock {

    public ModTrapDoor(Properties prop, SoundType sound, float hardness, float resistance) {
        super(prop.sound(sound).hardnessAndResistance(hardness, resistance));
    }


}

package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.subsystem.AntennaSubsystem;

public class BeaconBlock extends Block {

	public BeaconBlock() {
		super(Prop.Blocks.BASIC_TECH.get());
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		if(!worldIn.isRemote && handIn == player.getActiveHand()) {
			for(DimensionType type : DimensionType.getAll()) {
				if(DimensionType.getKey(type).getNamespace().contentEquals(Tardis.MODID)) {
					TardisHelper.getConsoleInWorld(worldIn.getServer().getWorld(type)).ifPresent(tile -> {
						tile.getSubsystem(AntennaSubsystem.class).ifPresent(ant -> {
							if(ant.canBeUsed()) {
								tile.addDistressSignal(new SpaceTimeCoord(player.dimension, pos));
							}
						});
					});
				}
			}
		}
		return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
	}

}

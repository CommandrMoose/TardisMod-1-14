package net.tardis.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.containers.ReclamationContainer;
import net.tardis.mod.misc.ContainerProvider;
import net.tardis.mod.misc.IDontBreak;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.tileentities.ReclamationTile;

public class ReclamationBlock extends TileBlock implements IDontBreak{

	public ReclamationBlock() {
		super(Prop.Blocks.BASIC_TECH.get().hardnessAndResistance(999F));
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		if(!worldIn.isRemote) {
			NetworkHooks.openGui((ServerPlayerEntity)player,
					new ContainerProvider<ReclamationContainer>((int id, PlayerInventory inv, PlayerEntity play) ->
						new ReclamationContainer(id, inv, (ReclamationTile)worldIn.getTileEntity(pos))), buf -> buf.writeBlockPos(pos));
		}
		return true;
	}

}

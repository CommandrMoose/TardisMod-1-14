package net.tardis.mod.contexts.gui;

import net.tardis.mod.misc.GuiContext;

public class GuiContextEnum<T extends Enum<?>> extends GuiContext {

	private T enumVar;
	
	public GuiContextEnum(T enumVar) {
		this.enumVar = enumVar;
	}
	
	public T get() {
		return this.enumVar;
	}
}

package net.tardis.mod.client.renderers;

import javax.annotation.Nonnull;

import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;


public interface TGetPlayerRenderer {
	
	  
		@OnlyIn(Dist.CLIENT)
	  default @Nonnull TRenderHandler getRender() {
	    return TRenderHelper.NULL_RENDERER;
	  }
	
	  @OnlyIn(Dist.CLIENT)
	  default @Nonnull TRenderHandler getRender(@Nonnull AbstractClientPlayerEntity player) {
		  return getRender();
	  }
	}

package net.tardis.mod.client.renderers;
import javax.annotation.Nonnull;

import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.entity.PlayerRenderer;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public interface TRenderHelper {

	 /**
	   * Used when conditionally rendering an item
	   */
	
	 @OnlyIn(Dist.CLIENT)
	  public static final @Nonnull TRenderHandler NULL_RENDERER = new TRenderHandler() {
	  public void doRenderLayer(@Nonnull PlayerRenderer renderPlayer, Inventory inventory, @Nonnull ItemStack item,
		      @Nonnull AbstractClientPlayerEntity entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw,
		      float headPitch, float scale) {
		  
	  }
	};
}

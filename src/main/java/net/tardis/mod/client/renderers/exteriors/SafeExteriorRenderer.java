package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.SafeExteriorModel;
import net.tardis.mod.tileentities.exteriors.SafeExteriorTile;

public class SafeExteriorRenderer extends ExteriorRenderer<SafeExteriorTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/safe.png");
	public static final SafeExteriorModel MODEL = new SafeExteriorModel();
	
	@Override
	public void renderExterior(SafeExteriorTile tile) {
		GlStateManager.translated(0, -0.25, 0);
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		MODEL.render(tile);
	}

}

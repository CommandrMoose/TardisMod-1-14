package net.tardis.mod.client.models.interiordoors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.renderers.exteriors.SafeExteriorRenderer;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.misc.IDoorType.EnumDoorType;

// Made with Blockbench 3.5.2
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class SafeInteriorModel extends Model implements IInteriorDoorRenderer{
	
	private final RendererModel door;
	private final RendererModel frame;

	public SafeInteriorModel() {
		textureWidth = 256;
		textureHeight = 256;

		door = new RendererModel(this);
		door.setRotationPoint(14.0F, -9.6667F, -11.6375F);
		door.cubeList.add(new ModelBox(door, 80, 0, -28.0F, -44.3333F, -1.1625F, 28, 78, 2, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 0, 112, -28.025F, -44.3333F, -1.1875F, 28, 78, 0, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 104, 101, -27.5F, -18.3333F, 0.8375F, 6, 25, 1, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 118, 130, -1.0F, -38.3333F, -1.0625F, 2, 12, 2, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 118, 130, -1.0F, 15.6667F, -1.0625F, 2, 12, 2, 0.0F, false));

		frame = new RendererModel(this);
		frame.setRotationPoint(0.0F, 24.0F, -12.0F);
		frame.cubeList.add(new ModelBox(frame, 116, 155, -18.0F, -80.0F, 0.0F, 4, 80, 2, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 140, 0, -14.0F, -80.0F, 0.0F, 28, 2, 2, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 116, 155, 14.0F, -80.0F, 0.0F, 4, 80, 2, 0.0F, false));
	}

	public void render(DoorEntity entity) {
		GlStateManager.pushMatrix();
		GlStateManager.enableRescaleNormal();
		GlStateManager.translated(0, 0.7, -0.2);
		GlStateManager.scaled(0.5, 0.5, 0.5);
		
		this.door.rotateAngleY = (float) Math.toRadians(EnumDoorType.SAFE.getRotationForState(entity.getOpenState()));
		
		door.render(0.0625F);
		frame.render(0.0625F);
		GlStateManager.popMatrix();
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public ResourceLocation getTexture() {
		return SafeExteriorRenderer.TEXTURE;
	}
}
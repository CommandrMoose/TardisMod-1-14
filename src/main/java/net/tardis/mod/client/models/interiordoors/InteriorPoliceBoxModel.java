package net.tardis.mod.client.models.interiordoors;



import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.ModernPoliceBoxExteriorModel;
import net.tardis.mod.client.renderers.exteriors.ModernPoliceBoxExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.PoliceBoxExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.SteamExteriorRenderer;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.ConsoleTile;

public class InteriorPoliceBoxModel extends Model implements IInteriorDoorRenderer{
	
	/*Texture of the door. The interior door will be able to dynamically change its model,
	*so ensure the file path of its texture is correct or you will crash the game.
	*Normally, you would put the interior door's texture in a sub folder within the exterior folder,
	*but in our case the door uses the same texture as the exterior.
	*/

	private final RendererModel Newbery;
	private final RendererModel CornerPosts;
	private final RendererModel UpperSignage;
	private final RendererModel Trim;
	private final RendererModel RoofStacks;
	private final RendererModel Lamp;
	private final RendererModel LeftDoor;
	private final RendererModel bone4;
	private final RendererModel bone5;
	private final RendererModel RightDoor;
	private final RendererModel bone6;
	private final RendererModel boti;

	public InteriorPoliceBoxModel() {
		textureWidth = 512;
		textureHeight = 512;

		Newbery = new RendererModel(this);
		Newbery.setRotationPoint(0.0F, 24.0F, 0.0F);

		CornerPosts = new RendererModel(this);
		CornerPosts.setRotationPoint(0.0F, 0.0F, 0.0F);
		Newbery.addChild(CornerPosts);
		CornerPosts.cubeList.add(new ModelBox(CornerPosts, 82, 131, 15.5F, -69.0F, -20.5F, 5, 70, 5, 0.0F, false));
		CornerPosts.cubeList.add(new ModelBox(CornerPosts, 82, 131, -20.5F, -69.0F, -20.5F, 5, 70, 5, 0.0F, false));

		UpperSignage = new RendererModel(this);
		UpperSignage.setRotationPoint(0.0F, 0.0F, 0.0F);
		Newbery.addChild(UpperSignage);
		UpperSignage.cubeList.add(new ModelBox(UpperSignage, 160, 138, -18.5F, -66.0F, -22.5F, 37, 4, 4, 0.0F, false));

		Trim = new RendererModel(this);
		Trim.setRotationPoint(0.0F, 0.0F, 0.0F);
		Newbery.addChild(Trim);
		Trim.cubeList.add(new ModelBox(Trim, 210, 212, 14.5F, -61.0F, -18.5F, 4, 62, 2, 0.0F, false));
		Trim.cubeList.add(new ModelBox(Trim, 210, 212, -18.5F, -61.0F, -18.5F, 4, 62, 2, 0.0F, false));
		Trim.cubeList.add(new ModelBox(Trim, 149, 127, -18.5F, -62.0F, -18.5F, 37, 1, 2, 0.0F, false));

		RoofStacks = new RendererModel(this);
		RoofStacks.setRotationPoint(0.0F, 0.0F, 0.0F);
		Newbery.addChild(RoofStacks);
		RoofStacks.cubeList.add(new ModelBox(RoofStacks, 35, 228, -19.5F, -69.0F, -19.5F, 39, 3, 3, 0.0F, false));

		Lamp = new RendererModel(this);
		Lamp.setRotationPoint(0.0F, 0.0F, 0.0F);
		Newbery.addChild(Lamp);

		LeftDoor = new RendererModel(this);
		LeftDoor.setRotationPoint(14.5F, -3.0F, -17.5F);
		Newbery.addChild(LeftDoor);
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 45, 135, -12.0F, 2.0F, 0.0F, 10, 2, 2, 0.0F, true));
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 0, 120, -12.0F, -13.0F, 0.0F, 10, 2, 2, 0.0F, true));
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 0, 120, -12.0F, -28.0F, 0.0F, 10, 2, 2, 0.0F, true));
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 0, 120, -12.0F, -43.0F, 0.0F, 10, 2, 2, 0.0F, true));
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 0, 120, -12.0F, -58.0F, 0.0F, 10, 2, 2, 0.0F, true));
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 10, 215, -15.0F, -58.0F, 0.0F, 3, 62, 2, 0.0F, true));
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 234, 146, -15.0F, -58.0F, -1.0F, 1, 62, 1, 0.0F, true));
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 230, 146, -15.0F, -58.0F, 2.0F, 1, 62, 1, 0.0F, true));
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 232, 224, -2.0F, -58.0F, 0.0F, 2, 62, 2, 0.0F, true));
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 20, 0, -12.0F, -26.0F, 1.1F, 10, 28, 0, 0.0F, true));
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 0, 131, -12.0F, -26.0F, 0.9F, 10, 28, 0, 0.0F, true));

		bone4 = new RendererModel(this);
		bone4.setRotationPoint(-14.5F, -47.0F, 1.0F);
		LeftDoor.addChild(bone4);
		bone4.cubeList.add(new ModelBox(bone4, 0, 106, 2.5F, -9.0F, 0.0F, 10, 13, 1, 0.0F, true));

		bone5 = new RendererModel(this);
		bone5.setRotationPoint(-2.0F, -32.0F, 0.0F);
		LeftDoor.addChild(bone5);
		bone5.cubeList.add(new ModelBox(bone5, 0, 64, -10.0F, -9.0F, 0.0F, 10, 13, 1, 0.0F, true));

		RightDoor = new RendererModel(this);
		RightDoor.setRotationPoint(-14.5F, -3.0F, -17.5F);
		Newbery.addChild(RightDoor);
		RightDoor.cubeList.add(new ModelBox(RightDoor, 45, 135, 2.0F, 2.0F, 0.0F, 10, 2, 2, 0.0F, true));
		RightDoor.cubeList.add(new ModelBox(RightDoor, 0, 120, 2.0F, -13.0F, 0.0F, 10, 2, 2, 0.0F, true));
		RightDoor.cubeList.add(new ModelBox(RightDoor, 0, 120, 2.0F, -28.0F, 0.0F, 10, 2, 2, 0.0F, true));
		RightDoor.cubeList.add(new ModelBox(RightDoor, 0, 120, 2.0F, -43.0F, 0.0F, 10, 2, 2, 0.0F, true));
		RightDoor.cubeList.add(new ModelBox(RightDoor, 0, 120, 2.0F, -58.0F, 0.0F, 10, 2, 2, 0.0F, true));
		RightDoor.cubeList.add(new ModelBox(RightDoor, 232, 224, 12.0F, -58.0F, 0.0F, 2, 62, 2, 0.0F, true));
		RightDoor.cubeList.add(new ModelBox(RightDoor, 232, 224, 0.0F, -58.0F, 0.0F, 2, 62, 2, 0.0F, true));
		RightDoor.cubeList.add(new ModelBox(RightDoor, 0, 0, 2.0F, -41.0F, 1.1F, 10, 43, 0, 0.0F, true));
		RightDoor.cubeList.add(new ModelBox(RightDoor, 0, 131, 2.0F, -41.0F, 0.9F, 10, 43, 0, 0.0F, true));

		bone6 = new RendererModel(this);
		bone6.setRotationPoint(14.5F, -43.0F, 1.0F);
		RightDoor.addChild(bone6);
		bone6.cubeList.add(new ModelBox(bone6, 0, 106, -12.5F, -13.0F, 0.0F, 10, 13, 1, 0.0F, true));

		boti = new RendererModel(this);
		boti.setRotationPoint(0.0F, 24.0F, 0.0F);
		boti.cubeList.add(new ModelBox(boti, 0, 297, -14.55F, -61.0F, -16.45F, 29, 62, 2, 0.0F, false));
	}

	public void setRotationAngle(RendererModel RendererModel, float x, float y, float z) {
		RendererModel.rotateAngleX = x;
		RendererModel.rotateAngleY = y;
		RendererModel.rotateAngleZ = z;
	}

	@Override
	public void render(DoorEntity door) {
		GlStateManager.pushMatrix();//Tell rendering engine to add a new object to the render matrix stack
		GlStateManager.rotated(180, 0, 1, 0);
		GlStateManager.translated(0, 0.72, 1.05);
		GlStateManager.enableRescaleNormal(); //Ensures lighting on model is preserved when rescaled
		GlStateManager.scaled(0.5, 0.5, 0.5);
		EnumDoorState state = door.getOpenState();
		
		switch(state) {
		case ONE:
			this.RightDoor.rotateAngleY = (float) Math.toRadians(
					EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.ONE)); //Only open right door, left door is closed by default
			this.LeftDoor.rotateAngleY = (float) Math.toRadians(
					EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.CLOSED)); 
			break;
		case BOTH:
			this.RightDoor.rotateAngleY = (float) Math.toRadians(
					EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.ONE)); 
			this.LeftDoor.rotateAngleY = (float) Math.toRadians(
					EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.BOTH)); //Open left door,Right door is already open
			break;
		case CLOSED://close both doors
			this.RightDoor.rotateAngleY = (float) Math.toRadians(
					EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.CLOSED)); 
			this.LeftDoor.rotateAngleY = (float) Math.toRadians(
					EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
			break;
		default:
			break;
		
		}
		Newbery.render(0.0625F);
		boti.render(0.0625F);
		
		GlStateManager.disableRescaleNormal();//Ensures lighting on model is preserved when rescaled
		
		GlStateManager.popMatrix(); //Finish rendering
	}

	@Override
	public ResourceLocation getTexture() {
		ConsoleTile tile = TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).orElse(null);
		if(tile != null) {
			int index = tile.getExteriorManager().getExteriorVariant();
			if(tile.getExterior().getVariants() != null && index < tile.getExterior().getVariants().length)
				return tile.getExterior().getVariants()[index].getTexture();
		}
		return PoliceBoxExteriorRenderer.TEXTURE;
	}
}
package net.tardis.mod.client.models.consoles;

import net.tardis.mod.tileentities.ConsoleTile;

public interface IConsoleModel {

	void render(ConsoleTile console, float scale);
}

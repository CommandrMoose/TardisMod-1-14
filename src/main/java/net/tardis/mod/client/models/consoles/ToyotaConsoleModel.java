package net.tardis.mod.client.models.consoles;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.tileentities.ConsoleTile;

// Made with Blockbench 3.5.2
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class ToyotaConsoleModel extends Model {
	private final RendererModel glow;
	private final RendererModel rotorlights;
	private final RendererModel bone72;
	private final RendererModel bone73;
	private final RendererModel bone74;
	private final RendererModel bone75;
	private final RendererModel bone76;
	private final RendererModel rotor2;
	private final RendererModel telepathic;
	private final RendererModel rotation4;
	private final RendererModel Telepathiccircuits;
	private final RendererModel monitor3;
	private final RendererModel rotation8;
	private final RendererModel bone110;
	private final RendererModel monitor2;
	private final RendererModel telepathic2;
	private final RendererModel rotation9;
	private final RendererModel Dimensioncontrol2;
	private final RendererModel borders;
	private final RendererModel bone2;
	private final RendererModel bone3;
	private final RendererModel bone4;
	private final RendererModel bone5;
	private final RendererModel bone6;
	private final RendererModel edges2;
	private final RendererModel bone7;
	private final RendererModel bone19;
	private final RendererModel bone31;
	private final RendererModel bone32;
	private final RendererModel bone65;
	private final RendererModel bone71;
	private final RendererModel bone77;
	private final RendererModel bone78;
	private final RendererModel bone79;
	private final RendererModel bone80;
	private final RendererModel bone81;
	private final RendererModel edges;
	private final RendererModel bone33;
	private final RendererModel bone34;
	private final RendererModel bone35;
	private final RendererModel bone36;
	private final RendererModel bone37;
	private final RendererModel bone38;
	private final RendererModel bone39;
	private final RendererModel bone40;
	private final RendererModel bone41;
	private final RendererModel bone42;
	private final RendererModel bone43;
	private final RendererModel panels2;
	private final RendererModel bone82;
	private final RendererModel bone;
	private final RendererModel bone83;
	private final RendererModel bone84;
	private final RendererModel bone85;
	private final RendererModel bone86;
	private final RendererModel bone87;
	private final RendererModel bone88;
	private final RendererModel bone89;
	private final RendererModel bone90;
	private final RendererModel bone91;
	private final RendererModel panels;
	private final RendererModel bone13;
	private final RendererModel bone50;
	private final RendererModel bone8;
	private final RendererModel bone14;
	private final RendererModel bone9;
	private final RendererModel bone15;
	private final RendererModel bone10;
	private final RendererModel bone16;
	private final RendererModel bone49;
	private final RendererModel bone11;
	private final RendererModel bone17;
	private final RendererModel bone12;
	private final RendererModel bone18;
	private final RendererModel bone64;
	private final RendererModel topedges;
	private final RendererModel bone20;
	private final RendererModel bone21;
	private final RendererModel bone22;
	private final RendererModel bone23;
	private final RendererModel bone24;
	private final RendererModel bone25;
	private final RendererModel bone26;
	private final RendererModel bone27;
	private final RendererModel bone28;
	private final RendererModel bone29;
	private final RendererModel bone30;
	private final RendererModel centre;
	private final RendererModel bone66;
	private final RendererModel bone67;
	private final RendererModel bone68;
	private final RendererModel bone69;
	private final RendererModel bone70;
	private final RendererModel base;
	private final RendererModel bone97;
	private final RendererModel bone98;
	private final RendererModel bone99;
	private final RendererModel bone100;
	private final RendererModel bone101;
	private final RendererModel NorthWest;
	private final RendererModel rotation7;
	private final RendererModel Dimensioncontrol;
	private final RendererModel bone108;
	private final RendererModel fastreturn;
	private final RendererModel bone107;
	private final RendererModel bone46;
	private final RendererModel bone47;
	private final RendererModel bone103;
	private final RendererModel facing_rotate_x;
	private final RendererModel NorthEast;
	private final RendererModel rotation3;
	private final RendererModel refueller;
	private final RendererModel bone48;
	private final RendererModel randomiser;
	private final RendererModel South;
	private final RendererModel rotation5;
	private final RendererModel throttle_rotate_x;
	private final RendererModel handbrake_rotate_y;
	private final RendererModel bone55;
	private final RendererModel stabiliser_translate_y;
	private final RendererModel bone54;
	private final RendererModel ycontrol;
	private final RendererModel xcontrol;
	private final RendererModel zcontrol;
	private final RendererModel bone95;
	private final RendererModel SouthWest;
	private final RendererModel rotation6;
	private final RendererModel bone45;
	private final RendererModel landtype_translate_z;
	private final RendererModel bone105;
	private final RendererModel increment_translate_z;
	private final RendererModel communicator;
	private final RendererModel North;
	private final RendererModel rotation2;
	private final RendererModel bone96;
	private final RendererModel Door_rotate_x;
	private final RendererModel bone51;
	private final RendererModel sonicport;
	private final RendererModel bone60;
	private final RendererModel bone57;
	private final RendererModel bone58;
	private final RendererModel bone61;
	private final RendererModel bone62;
	private final RendererModel rotor;
	private final RendererModel top_translate_y;
	private final RendererModel bottom_translate_y;

	public ToyotaConsoleModel() {
		textureWidth = 128;
		textureHeight = 128;

		glow = new RendererModel(this);
		glow.setRotationPoint(0.0F, 3.0F, 0.0F);
		

		rotorlights = new RendererModel(this);
		rotorlights.setRotationPoint(0.0F, -13.0F, 0.0F);
		glow.addChild(rotorlights);
		rotorlights.cubeList.add(new ModelBox(rotorlights, 116, 42, -1.5F, -57.5F, 6.4F, 3, 54, 3, 0.0F, false));
		rotorlights.cubeList.add(new ModelBox(rotorlights, 116, 55, -1.5F, 20.5F, 6.4F, 3, 11, 3, 0.0F, false));

		bone72 = new RendererModel(this);
		bone72.setRotationPoint(0.0F, 0.0F, 0.0F);
		rotorlights.addChild(bone72);
		setRotationAngle(bone72, 0.0F, -1.0472F, 0.0F);
		bone72.cubeList.add(new ModelBox(bone72, 116, 42, -1.5F, -57.5F, 6.4F, 3, 54, 3, 0.0F, false));
		bone72.cubeList.add(new ModelBox(bone72, 116, 55, -1.5F, 20.5F, 6.4F, 3, 11, 3, 0.0F, false));

		bone73 = new RendererModel(this);
		bone73.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone72.addChild(bone73);
		setRotationAngle(bone73, 0.0F, -1.0472F, 0.0F);
		bone73.cubeList.add(new ModelBox(bone73, 116, 42, -1.5F, -57.5F, 6.4F, 3, 54, 3, 0.0F, false));
		bone73.cubeList.add(new ModelBox(bone73, 116, 55, -1.5F, 20.5F, 6.4F, 3, 11, 3, 0.0F, false));

		bone74 = new RendererModel(this);
		bone74.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone73.addChild(bone74);
		setRotationAngle(bone74, 0.0F, -1.0472F, 0.0F);
		bone74.cubeList.add(new ModelBox(bone74, 116, 42, -1.5F, -57.5F, 6.4F, 3, 54, 3, 0.0F, false));
		bone74.cubeList.add(new ModelBox(bone74, 116, 55, -1.5F, 20.5F, 6.4F, 3, 11, 3, 0.0F, false));

		bone75 = new RendererModel(this);
		bone75.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone74.addChild(bone75);
		setRotationAngle(bone75, 0.0F, -1.0472F, 0.0F);
		bone75.cubeList.add(new ModelBox(bone75, 116, 42, -1.5F, -57.5F, 6.4F, 3, 54, 3, 0.0F, false));
		bone75.cubeList.add(new ModelBox(bone75, 116, 55, -1.5F, 20.5F, 6.4F, 3, 11, 3, 0.0F, false));

		bone76 = new RendererModel(this);
		bone76.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone75.addChild(bone76);
		setRotationAngle(bone76, 0.0F, -1.0472F, 0.0F);
		bone76.cubeList.add(new ModelBox(bone76, 116, 42, -1.5F, -57.5F, 6.4F, 3, 54, 3, 0.0F, false));
		bone76.cubeList.add(new ModelBox(bone76, 116, 55, -1.5F, 20.5F, 6.4F, 3, 11, 3, 0.0F, false));

		rotor2 = new RendererModel(this);
		rotor2.setRotationPoint(0.0F, -13.0F, 0.0F);
		glow.addChild(rotor2);
		rotor2.cubeList.add(new ModelBox(rotor2, 116, 109, -1.5F, -17.5F, -1.6F, 3, 14, 3, 0.0F, false));
		rotor2.cubeList.add(new ModelBox(rotor2, 107, 110, -1.0F, -27.5F, -1.1F, 2, 14, 2, 0.0F, false));
		rotor2.cubeList.add(new ModelBox(rotor2, 107, 110, -1.0F, -45.5F, -1.1F, 2, 14, 2, 0.0F, false));
		rotor2.cubeList.add(new ModelBox(rotor2, 116, 109, -1.5F, -55.5F, -1.6F, 3, 14, 3, 0.0F, false));

		telepathic = new RendererModel(this);
		telepathic.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow.addChild(telepathic);
		setRotationAngle(telepathic, 0.0F, 2.0944F, 0.0F);
		

		rotation4 = new RendererModel(this);
		rotation4.setRotationPoint(0.0F, 0.0F, 0.0F);
		telepathic.addChild(rotation4);
		setRotationAngle(rotation4, 0.3491F, 0.0F, 0.0F);
		

		Telepathiccircuits = new RendererModel(this);
		Telepathiccircuits.setRotationPoint(0.0F, 0.8966F, -4.3813F);
		rotation4.addChild(Telepathiccircuits);
		Telepathiccircuits.cubeList.add(new ModelBox(Telepathiccircuits, 0, 57, 4.0F, -18.9868F, -12.1305F, 4, 1, 3, 0.0F, false));
		Telepathiccircuits.cubeList.add(new ModelBox(Telepathiccircuits, 43, 57, 1.0F, -18.9868F, -16.1305F, 5, 1, 2, 0.0F, false));
		Telepathiccircuits.cubeList.add(new ModelBox(Telepathiccircuits, 0, 57, 6.0F, -18.9868F, -17.1305F, 4, 1, 3, 0.0F, false));
		Telepathiccircuits.cubeList.add(new ModelBox(Telepathiccircuits, 43, 57, 1.0F, -18.9868F, -12.1305F, 3, 1, 2, 0.0F, false));
		Telepathiccircuits.cubeList.add(new ModelBox(Telepathiccircuits, 41, 61, -8.0F, -18.9868F, -12.1305F, 4, 1, 3, 0.0F, false));
		Telepathiccircuits.cubeList.add(new ModelBox(Telepathiccircuits, 30, 57, -4.0F, -18.9868F, -12.1305F, 3, 1, 2, 0.0F, false));
		Telepathiccircuits.cubeList.add(new ModelBox(Telepathiccircuits, 30, 57, -6.0F, -18.9868F, -16.1305F, 5, 1, 2, 0.0F, false));
		Telepathiccircuits.cubeList.add(new ModelBox(Telepathiccircuits, 41, 61, -10.0F, -18.9868F, -17.1305F, 4, 1, 3, 0.0F, false));
		Telepathiccircuits.cubeList.add(new ModelBox(Telepathiccircuits, 23, 68, -12.0F, -18.9868F, -22.1305F, 8, 1, 3, 0.0F, false));
		Telepathiccircuits.cubeList.add(new ModelBox(Telepathiccircuits, 27, 55, -9.0F, -18.9868F, -28.1305F, 6, 1, 4, 0.0F, false));
		Telepathiccircuits.cubeList.add(new ModelBox(Telepathiccircuits, 11, 57, -16.0F, -18.9868F, -29.1305F, 7, 1, 5, 0.0F, false));
		Telepathiccircuits.cubeList.add(new ModelBox(Telepathiccircuits, 11, 57, 9.0F, -18.9868F, -29.1305F, 7, 1, 5, 0.0F, false));
		Telepathiccircuits.cubeList.add(new ModelBox(Telepathiccircuits, 21, 64, 4.0F, -18.9868F, -22.1305F, 8, 1, 3, 0.0F, false));
		Telepathiccircuits.cubeList.add(new ModelBox(Telepathiccircuits, 0, 64, -4.0F, -18.9868F, -23.1305F, 8, 1, 5, 0.0F, false));
		Telepathiccircuits.cubeList.add(new ModelBox(Telepathiccircuits, 4, 67, -3.0F, -18.9868F, -27.1305F, 6, 1, 2, 0.0F, false));
		Telepathiccircuits.cubeList.add(new ModelBox(Telepathiccircuits, 40, 55, 3.0F, -18.9868F, -28.1305F, 6, 1, 4, 0.0F, false));

		monitor3 = new RendererModel(this);
		monitor3.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow.addChild(monitor3);
		setRotationAngle(monitor3, 0.0F, 1.0472F, 0.0F);
		

		rotation8 = new RendererModel(this);
		rotation8.setRotationPoint(0.0F, 0.0F, 0.0F);
		monitor3.addChild(rotation8);
		setRotationAngle(rotation8, 0.3491F, 0.0F, 0.0F);
		

		bone110 = new RendererModel(this);
		bone110.setRotationPoint(0.0F, 0.8966F, -4.3813F);
		rotation8.addChild(bone110);
		bone110.cubeList.add(new ModelBox(bone110, 10, 50, -4.1F, -18.7368F, -13.1305F, 8, 1, 1, 0.0F, false));
		bone110.cubeList.add(new ModelBox(bone110, 10, 50, -7.1F, -18.7368F, -14.1305F, 3, 1, 1, 0.0F, false));
		bone110.cubeList.add(new ModelBox(bone110, 10, 50, 3.9F, -18.7368F, -14.1305F, 3, 1, 1, 0.0F, false));
		bone110.cubeList.add(new ModelBox(bone110, 10, 50, -4.1F, -18.7368F, -26.1305F, 8, 1, 1, 0.0F, false));
		bone110.cubeList.add(new ModelBox(bone110, 10, 50, -7.1F, -18.7368F, -25.1305F, 3, 1, 1, 0.0F, false));
		bone110.cubeList.add(new ModelBox(bone110, 10, 50, 3.9F, -18.7368F, -25.1305F, 3, 1, 1, 0.0F, false));
		bone110.cubeList.add(new ModelBox(bone110, 10, 48, 9.9F, -18.7368F, -21.1305F, 1, 1, 4, 0.0F, false));
		bone110.cubeList.add(new ModelBox(bone110, 10, 50, -9.1F, -18.7368F, -24.1305F, 2, 1, 1, 0.0F, false));
		bone110.cubeList.add(new ModelBox(bone110, 10, 50, 6.9F, -18.7368F, -24.1305F, 2, 1, 1, 0.0F, false));
		bone110.cubeList.add(new ModelBox(bone110, 10, 50, -9.1F, -18.7368F, -15.1305F, 2, 1, 1, 0.0F, false));
		bone110.cubeList.add(new ModelBox(bone110, 10, 50, 6.9F, -18.7368F, -15.1305F, 2, 1, 1, 0.0F, false));
		bone110.cubeList.add(new ModelBox(bone110, 10, 50, -10.1F, -18.7368F, -17.1305F, 1, 1, 2, 0.0F, false));
		bone110.cubeList.add(new ModelBox(bone110, 10, 50, 8.9F, -18.7368F, -17.1305F, 1, 1, 2, 0.0F, false));
		bone110.cubeList.add(new ModelBox(bone110, 10, 50, -10.1F, -18.7368F, -23.1305F, 1, 1, 2, 0.0F, false));
		bone110.cubeList.add(new ModelBox(bone110, 10, 50, 8.9F, -18.7368F, -23.1305F, 1, 1, 2, 0.0F, false));
		bone110.cubeList.add(new ModelBox(bone110, 10, 48, -11.0F, -18.7368F, -21.1305F, 1, 1, 4, 0.0F, false));

		monitor2 = new RendererModel(this);
		monitor2.setRotationPoint(-0.1F, 0.0F, 0.0F);
		bone110.addChild(monitor2);
		monitor2.cubeList.add(new ModelBox(monitor2, 0, 24, -11.0F, -18.5368F, -26.1305F, 22, 0, 14, 0.0F, false));

		telepathic2 = new RendererModel(this);
		telepathic2.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow.addChild(telepathic2);
		setRotationAngle(telepathic2, 0.0F, -1.0472F, 0.0F);
		

		rotation9 = new RendererModel(this);
		rotation9.setRotationPoint(0.0F, 0.0F, 0.0F);
		telepathic2.addChild(rotation9);
		setRotationAngle(rotation9, 0.3491F, 0.0F, 0.0F);
		

		Dimensioncontrol2 = new RendererModel(this);
		Dimensioncontrol2.setRotationPoint(0.0F, 0.8966F, -4.3813F);
		rotation9.addChild(Dimensioncontrol2);
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 59, 14, -10.0F, -18.7368F, -22.1305F, 7, 1, 6, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 63, 17, -9.0F, -18.7368F, -25.1305F, 6, 1, 3, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 66, 17, -6.0F, -18.7368F, -28.1305F, 3, 1, 3, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 68, 18, -8.0F, -18.7368F, -27.1305F, 2, 1, 2, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 63, 17, -9.0F, -18.7368F, -16.1305F, 6, 1, 3, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 68, 18, -8.0F, -18.7368F, -13.1305F, 2, 1, 2, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 66, 17, -6.0F, -18.7368F, -13.1305F, 3, 1, 3, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 66, 17, 3.0F, -18.7368F, -13.1305F, 3, 1, 3, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 46, 0, -3.0F, -18.7368F, -29.1305F, 6, 1, 20, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 63, 17, 3.0F, -18.7368F, -16.1305F, 6, 1, 3, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 68, 18, 6.0F, -18.7368F, -13.1305F, 2, 1, 2, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 59, 14, 3.0F, -18.7368F, -22.1305F, 7, 1, 6, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 63, 17, 3.0F, -18.7368F, -25.1305F, 6, 1, 3, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 68, 18, 6.0F, -18.7368F, -27.1305F, 2, 1, 2, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 66, 17, 3.0F, -18.7368F, -28.1305F, 3, 1, 3, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 101, 21, 3.0F, -18.9868F, -28.6305F, 3, 1, 1, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 101, 21, 6.0F, -18.9868F, -27.6305F, 2, 1, 1, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 101, 21, 7.5F, -18.9868F, -27.1305F, 1, 1, 2, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 104, 19, 8.5F, -18.9868F, -25.1305F, 1, 1, 3, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 101, 16, 9.5F, -18.9868F, -22.1305F, 1, 1, 6, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 104, 19, 8.5F, -18.9868F, -16.1305F, 1, 1, 3, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 101, 21, 7.5F, -18.9868F, -13.1305F, 1, 1, 2, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 101, 21, 6.0F, -18.9868F, -11.6305F, 2, 1, 1, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 101, 21, 3.0F, -18.9868F, -10.6305F, 3, 1, 1, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 101, 21, -3.0F, -18.9868F, -9.6305F, 6, 1, 1, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 101, 21, -6.0F, -18.9868F, -10.6305F, 3, 1, 1, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 101, 21, -8.0F, -18.9868F, -11.6305F, 2, 1, 1, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 101, 21, -8.5F, -18.9868F, -13.1305F, 1, 1, 2, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 104, 19, -9.5F, -18.9868F, -16.1305F, 1, 1, 3, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 101, 16, -10.5F, -18.9868F, -22.1305F, 1, 1, 6, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 104, 19, -9.5F, -18.9868F, -25.1305F, 1, 1, 3, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 101, 21, -8.5F, -18.9868F, -27.1305F, 1, 1, 2, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 101, 21, -8.0F, -18.9868F, -27.6305F, 2, 1, 1, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 101, 21, -6.0F, -18.9868F, -28.6305F, 3, 1, 1, 0.0F, false));
		Dimensioncontrol2.cubeList.add(new ModelBox(Dimensioncontrol2, 101, 21, -3.0F, -18.9868F, -29.6305F, 6, 1, 1, 0.0F, false));

		borders = new RendererModel(this);
		borders.setRotationPoint(0.0F, 2.5F, 0.0F);
		borders.cubeList.add(new ModelBox(borders, 63, 76, -24.0F, -4.25F, 39.55F, 24, 2, 2, 0.0F, false));
		borders.cubeList.add(new ModelBox(borders, 63, 76, 0.0F, -4.25F, 39.55F, 24, 2, 2, 0.0F, false));
		borders.cubeList.add(new ModelBox(borders, 63, 76, 16.0F, -2.25F, 39.55F, 8, 2, 2, 0.0F, false));
		borders.cubeList.add(new ModelBox(borders, 63, 76, -4.0F, -2.25F, 39.55F, 8, 2, 2, 0.0F, false));
		borders.cubeList.add(new ModelBox(borders, 63, 76, -24.0F, -2.25F, 39.55F, 8, 2, 2, 0.0F, false));
		borders.cubeList.add(new ModelBox(borders, 63, 76, -24.0F, -2.275F, 38.0F, 24, 2, 2, 0.0F, false));
		borders.cubeList.add(new ModelBox(borders, 63, 76, 0.0F, -2.275F, 38.0F, 24, 2, 2, 0.0F, false));

		bone2 = new RendererModel(this);
		bone2.setRotationPoint(0.0F, 0.0F, 0.0F);
		borders.addChild(bone2);
		setRotationAngle(bone2, 0.0F, -1.0472F, 0.0F);
		bone2.cubeList.add(new ModelBox(bone2, 63, 76, -24.0F, -4.25F, 39.55F, 24, 2, 2, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 63, 76, 0.0F, -4.25F, 39.55F, 24, 2, 2, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 63, 76, 16.0F, -2.25F, 39.55F, 8, 2, 2, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 63, 76, -4.0F, -2.25F, 39.55F, 8, 2, 2, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 63, 76, -24.0F, -2.25F, 39.55F, 8, 2, 2, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 63, 76, -24.0F, -2.275F, 38.0F, 24, 2, 2, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 63, 76, 0.0F, -2.275F, 38.0F, 24, 2, 2, 0.0F, false));

		bone3 = new RendererModel(this);
		bone3.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone2.addChild(bone3);
		setRotationAngle(bone3, 0.0F, -1.0472F, 0.0F);
		bone3.cubeList.add(new ModelBox(bone3, 63, 76, -24.0F, -4.25F, 39.55F, 24, 2, 2, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 63, 76, 0.0F, -4.25F, 39.55F, 24, 2, 2, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 63, 76, 16.0F, -2.25F, 39.55F, 8, 2, 2, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 63, 76, -4.0F, -2.25F, 39.55F, 8, 2, 2, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 63, 76, -24.0F, -2.25F, 39.55F, 8, 2, 2, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 63, 76, -24.0F, -2.275F, 38.0F, 24, 2, 2, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 63, 76, 0.0F, -2.275F, 38.0F, 24, 2, 2, 0.0F, false));

		bone4 = new RendererModel(this);
		bone4.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone3.addChild(bone4);
		setRotationAngle(bone4, 0.0F, -1.0472F, 0.0F);
		bone4.cubeList.add(new ModelBox(bone4, 63, 76, 0.0F, -2.275F, 38.0F, 24, 2, 2, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 63, 76, -24.0F, -2.275F, 38.0F, 24, 2, 2, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 63, 76, -24.0F, -4.25F, 39.55F, 24, 2, 2, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 63, 76, 0.0F, -4.25F, 39.55F, 24, 2, 2, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 63, 76, 16.0F, -2.25F, 39.55F, 8, 2, 2, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 63, 76, -4.0F, -2.25F, 39.55F, 8, 2, 2, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 63, 76, -24.0F, -2.25F, 39.55F, 8, 2, 2, 0.0F, false));

		bone5 = new RendererModel(this);
		bone5.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone4.addChild(bone5);
		setRotationAngle(bone5, 0.0F, -1.0472F, 0.0F);
		bone5.cubeList.add(new ModelBox(bone5, 63, 76, -24.0F, -4.25F, 39.55F, 24, 2, 2, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 63, 76, 0.0F, -4.25F, 39.55F, 24, 2, 2, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 63, 76, 16.0F, -2.25F, 39.55F, 8, 2, 2, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 63, 76, -4.0F, -2.25F, 39.55F, 8, 2, 2, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 63, 76, -24.0F, -2.25F, 39.55F, 8, 2, 2, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 63, 76, -24.0F, -2.275F, 38.0F, 24, 2, 2, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 63, 76, 0.0F, -2.275F, 38.0F, 24, 2, 2, 0.0F, false));

		bone6 = new RendererModel(this);
		bone6.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone5.addChild(bone6);
		setRotationAngle(bone6, 0.0F, -1.0472F, 0.0F);
		bone6.cubeList.add(new ModelBox(bone6, 63, 76, -24.0F, -4.25F, 39.55F, 24, 2, 2, 0.0F, false));
		bone6.cubeList.add(new ModelBox(bone6, 63, 76, 0.0F, -4.25F, 39.55F, 24, 2, 2, 0.0F, false));
		bone6.cubeList.add(new ModelBox(bone6, 63, 76, 16.0F, -2.25F, 39.55F, 8, 2, 2, 0.0F, false));
		bone6.cubeList.add(new ModelBox(bone6, 63, 76, -7.0F, -2.25F, 39.55F, 11, 2, 2, 0.0F, false));
		bone6.cubeList.add(new ModelBox(bone6, 63, 76, -24.0F, -2.25F, 39.55F, 8, 2, 2, 0.0F, false));
		bone6.cubeList.add(new ModelBox(bone6, 63, 76, -24.0F, -2.275F, 38.0F, 24, 2, 2, 0.0F, false));
		bone6.cubeList.add(new ModelBox(bone6, 63, 76, 0.0F, -2.275F, 38.0F, 24, 2, 2, 0.0F, false));

		edges2 = new RendererModel(this);
		edges2.setRotationPoint(0.0F, -2.0F, 0.0F);
		setRotationAngle(edges2, 0.0F, -0.5236F, 0.0F);
		

		bone7 = new RendererModel(this);
		bone7.setRotationPoint(0.0F, 4.5F, 40.5F);
		edges2.addChild(bone7);
		setRotationAngle(bone7, 0.2269F, 0.0F, 0.0F);
		bone7.cubeList.add(new ModelBox(bone7, 84, 86, -1.5F, -1.2841F, -12.6088F, 3, 2, 19, 0.0F, false));
		bone7.cubeList.add(new ModelBox(bone7, 84, 86, -1.5F, -1.2841F, -30.6088F, 3, 2, 18, 0.0F, false));

		bone19 = new RendererModel(this);
		bone19.setRotationPoint(0.0F, 1.5F, 0.0F);
		edges2.addChild(bone19);
		setRotationAngle(bone19, 0.0F, -1.0472F, 0.0F);
		

		bone31 = new RendererModel(this);
		bone31.setRotationPoint(0.0F, 3.0F, 40.5F);
		bone19.addChild(bone31);
		setRotationAngle(bone31, 0.2269F, 0.0F, 0.0F);
		bone31.cubeList.add(new ModelBox(bone31, 84, 86, -1.5F, -1.2841F, -12.6088F, 3, 2, 19, 0.0F, false));
		bone31.cubeList.add(new ModelBox(bone31, 84, 86, -1.5F, -1.2841F, -30.6088F, 3, 2, 18, 0.0F, false));

		bone32 = new RendererModel(this);
		bone32.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone19.addChild(bone32);
		setRotationAngle(bone32, 0.0F, -1.0472F, 0.0F);
		

		bone65 = new RendererModel(this);
		bone65.setRotationPoint(0.0F, 3.0F, 40.5F);
		bone32.addChild(bone65);
		setRotationAngle(bone65, 0.2269F, 0.0F, 0.0F);
		bone65.cubeList.add(new ModelBox(bone65, 84, 86, -1.5F, -1.2841F, -12.6088F, 3, 2, 19, 0.0F, false));
		bone65.cubeList.add(new ModelBox(bone65, 84, 86, -1.5F, -1.2841F, -30.6088F, 3, 2, 18, 0.0F, false));

		bone71 = new RendererModel(this);
		bone71.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone32.addChild(bone71);
		setRotationAngle(bone71, 0.0F, -1.0472F, 0.0F);
		

		bone77 = new RendererModel(this);
		bone77.setRotationPoint(0.0F, 3.0F, 40.5F);
		bone71.addChild(bone77);
		setRotationAngle(bone77, 0.2269F, 0.0F, 0.0F);
		bone77.cubeList.add(new ModelBox(bone77, 84, 86, -1.5F, -1.2841F, -12.6088F, 3, 2, 19, 0.0F, false));
		bone77.cubeList.add(new ModelBox(bone77, 84, 86, -1.5F, -1.2841F, -30.6088F, 3, 2, 18, 0.0F, false));

		bone78 = new RendererModel(this);
		bone78.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone71.addChild(bone78);
		setRotationAngle(bone78, 0.0F, -1.0472F, 0.0F);
		

		bone79 = new RendererModel(this);
		bone79.setRotationPoint(0.0F, 3.0F, 40.5F);
		bone78.addChild(bone79);
		setRotationAngle(bone79, 0.2269F, 0.0F, 0.0F);
		bone79.cubeList.add(new ModelBox(bone79, 84, 86, -1.5F, -1.2841F, -12.6088F, 3, 2, 19, 0.0F, false));
		bone79.cubeList.add(new ModelBox(bone79, 84, 86, -1.5F, -1.2841F, -30.6088F, 3, 2, 18, 0.0F, false));

		bone80 = new RendererModel(this);
		bone80.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone78.addChild(bone80);
		setRotationAngle(bone80, 0.0F, -1.0472F, 0.0F);
		

		bone81 = new RendererModel(this);
		bone81.setRotationPoint(0.0F, 3.0F, 40.5F);
		bone80.addChild(bone81);
		setRotationAngle(bone81, 0.2269F, 0.0F, 0.0F);
		bone81.cubeList.add(new ModelBox(bone81, 84, 86, -1.5F, -1.2841F, -12.6088F, 3, 2, 19, 0.0F, false));
		bone81.cubeList.add(new ModelBox(bone81, 84, 86, -1.5F, -1.2841F, -30.6088F, 3, 2, 18, 0.0F, false));

		edges = new RendererModel(this);
		edges.setRotationPoint(0.0F, 1.5F, 0.0F);
		setRotationAngle(edges, 0.0F, -0.5236F, 0.0F);
		

		bone33 = new RendererModel(this);
		bone33.setRotationPoint(0.0F, -3.0F, 40.5F);
		edges.addChild(bone33);
		setRotationAngle(bone33, -0.3142F, 0.0F, 0.0F);
		bone33.cubeList.add(new ModelBox(bone33, 84, 87, -1.5F, -1.4659F, -11.6088F, 3, 2, 18, 0.0F, false));
		bone33.cubeList.add(new ModelBox(bone33, 84, 87, -1.5F, -1.4659F, -29.6088F, 3, 2, 18, 0.0F, false));

		bone34 = new RendererModel(this);
		bone34.setRotationPoint(0.0F, 0.0F, 0.0F);
		edges.addChild(bone34);
		setRotationAngle(bone34, 0.0F, -1.0472F, 0.0F);
		

		bone35 = new RendererModel(this);
		bone35.setRotationPoint(0.0F, -3.0F, 40.5F);
		bone34.addChild(bone35);
		setRotationAngle(bone35, -0.3142F, 0.0F, 0.0F);
		bone35.cubeList.add(new ModelBox(bone35, 84, 87, -1.5F, -1.4659F, -11.6088F, 3, 2, 18, 0.0F, false));
		bone35.cubeList.add(new ModelBox(bone35, 84, 87, -1.5F, -1.4659F, -29.6088F, 3, 2, 18, 0.0F, false));

		bone36 = new RendererModel(this);
		bone36.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone34.addChild(bone36);
		setRotationAngle(bone36, 0.0F, -1.0472F, 0.0F);
		

		bone37 = new RendererModel(this);
		bone37.setRotationPoint(0.0F, -3.0F, 40.5F);
		bone36.addChild(bone37);
		setRotationAngle(bone37, -0.3142F, 0.0F, 0.0F);
		bone37.cubeList.add(new ModelBox(bone37, 84, 87, -1.5F, -1.4659F, -11.6088F, 3, 2, 18, 0.0F, false));
		bone37.cubeList.add(new ModelBox(bone37, 84, 87, -1.5F, -1.4659F, -29.6088F, 3, 2, 18, 0.0F, false));

		bone38 = new RendererModel(this);
		bone38.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone36.addChild(bone38);
		setRotationAngle(bone38, 0.0F, -1.0472F, 0.0F);
		

		bone39 = new RendererModel(this);
		bone39.setRotationPoint(0.0F, -3.0F, 40.5F);
		bone38.addChild(bone39);
		setRotationAngle(bone39, -0.3142F, 0.0F, 0.0F);
		bone39.cubeList.add(new ModelBox(bone39, 84, 87, -1.5F, -1.4659F, -11.6088F, 3, 2, 18, 0.0F, false));
		bone39.cubeList.add(new ModelBox(bone39, 84, 87, -1.5F, -1.4659F, -29.6088F, 3, 2, 18, 0.0F, false));

		bone40 = new RendererModel(this);
		bone40.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone38.addChild(bone40);
		setRotationAngle(bone40, 0.0F, -1.0472F, 0.0F);
		

		bone41 = new RendererModel(this);
		bone41.setRotationPoint(0.0F, -3.0F, 40.5F);
		bone40.addChild(bone41);
		setRotationAngle(bone41, -0.3142F, 0.0F, 0.0F);
		bone41.cubeList.add(new ModelBox(bone41, 84, 87, -1.5F, -1.4659F, -11.6088F, 3, 2, 18, 0.0F, false));
		bone41.cubeList.add(new ModelBox(bone41, 84, 87, -1.5F, -1.4659F, -29.6088F, 3, 2, 18, 0.0F, false));

		bone42 = new RendererModel(this);
		bone42.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone40.addChild(bone42);
		setRotationAngle(bone42, 0.0F, -1.0472F, 0.0F);
		

		bone43 = new RendererModel(this);
		bone43.setRotationPoint(0.0F, -3.0F, 40.5F);
		bone42.addChild(bone43);
		setRotationAngle(bone43, -0.3142F, 0.0F, 0.0F);
		bone43.cubeList.add(new ModelBox(bone43, 84, 87, -1.5F, -1.4659F, -11.6088F, 3, 2, 18, 0.0F, false));
		bone43.cubeList.add(new ModelBox(bone43, 84, 87, -1.5F, -1.4659F, -29.6088F, 3, 2, 18, 0.0F, false));

		panels2 = new RendererModel(this);
		panels2.setRotationPoint(0.0F, -2.5F, 0.0F);
		

		bone82 = new RendererModel(this);
		bone82.setRotationPoint(0.0F, 3.5F, 40.5F);
		panels2.addChild(bone82);
		setRotationAngle(bone82, 0.2618F, 0.0F, 0.0F);
		bone82.cubeList.add(new ModelBox(bone82, 56, 34, -12.0F, -0.525F, -23.2F, 24, 1, 4, 0.0F, false));
		bone82.cubeList.add(new ModelBox(bone82, 56, 34, -14.0F, -0.525F, -19.2F, 28, 1, 4, 0.0F, false));
		bone82.cubeList.add(new ModelBox(bone82, 56, 34, -16.0F, -0.525F, -15.2F, 32, 1, 4, 0.0F, false));
		bone82.cubeList.add(new ModelBox(bone82, 56, 34, -18.0F, -0.525F, -11.2F, 36, 1, 4, 0.0F, false));
		bone82.cubeList.add(new ModelBox(bone82, 56, 34, -20.0F, -0.525F, -7.2F, 40, 1, 4, 0.0F, false));
		bone82.cubeList.add(new ModelBox(bone82, 56, 34, -22.0F, -0.525F, -3.2F, 44, 1, 2, 0.0F, false));
		bone82.cubeList.add(new ModelBox(bone82, 56, 34, -10.0F, -0.525F, -27.2F, 20, 1, 4, 0.0F, false));
		bone82.cubeList.add(new ModelBox(bone82, 56, 34, 6.0F, -1.525F, -25.2F, 1, 1, 3, 0.0F, false));
		bone82.cubeList.add(new ModelBox(bone82, 56, 34, -7.0F, -1.525F, -25.2F, 1, 1, 3, 0.0F, false));
		bone82.cubeList.add(new ModelBox(bone82, 56, 34, -6.0F, -1.525F, -26.2F, 12, 1, 1, 0.0F, false));
		bone82.cubeList.add(new ModelBox(bone82, 56, 34, -8.0F, -0.525F, -30.2F, 16, 1, 3, 0.0F, false));
		bone82.cubeList.add(new ModelBox(bone82, 56, 34, -6.0F, -0.525F, -32.2F, 12, 1, 2, 0.0F, false));

		bone = new RendererModel(this);
		bone.setRotationPoint(0.0F, 0.5F, 0.0F);
		panels2.addChild(bone);
		setRotationAngle(bone, 0.0F, -1.0472F, 0.0F);
		

		bone83 = new RendererModel(this);
		bone83.setRotationPoint(0.0F, 3.0F, 40.5F);
		bone.addChild(bone83);
		setRotationAngle(bone83, 0.2618F, 0.0F, 0.0F);
		bone83.cubeList.add(new ModelBox(bone83, 56, 34, -12.0F, -0.525F, -23.2F, 24, 1, 4, 0.0F, false));
		bone83.cubeList.add(new ModelBox(bone83, 56, 34, -14.0F, -0.525F, -19.2F, 28, 1, 4, 0.0F, false));
		bone83.cubeList.add(new ModelBox(bone83, 56, 34, -16.0F, -0.525F, -15.2F, 32, 1, 4, 0.0F, false));
		bone83.cubeList.add(new ModelBox(bone83, 56, 34, -18.0F, -0.525F, -11.2F, 36, 1, 4, 0.0F, false));
		bone83.cubeList.add(new ModelBox(bone83, 56, 34, -20.0F, -0.525F, -7.2F, 40, 1, 4, 0.0F, false));
		bone83.cubeList.add(new ModelBox(bone83, 56, 34, -22.0F, -0.525F, -3.2F, 44, 1, 2, 0.0F, false));
		bone83.cubeList.add(new ModelBox(bone83, 56, 34, -10.0F, -0.525F, -27.2F, 20, 1, 4, 0.0F, false));
		bone83.cubeList.add(new ModelBox(bone83, 56, 34, 6.0F, -1.525F, -25.2F, 1, 1, 3, 0.0F, false));
		bone83.cubeList.add(new ModelBox(bone83, 56, 34, -7.0F, -1.525F, -25.2F, 1, 1, 3, 0.0F, false));
		bone83.cubeList.add(new ModelBox(bone83, 56, 34, -6.0F, -1.525F, -26.2F, 12, 1, 1, 0.0F, false));
		bone83.cubeList.add(new ModelBox(bone83, 56, 34, -8.0F, -0.525F, -30.2F, 16, 1, 3, 0.0F, false));
		bone83.cubeList.add(new ModelBox(bone83, 56, 34, -6.0F, -0.525F, -32.2F, 12, 1, 2, 0.0F, false));

		bone84 = new RendererModel(this);
		bone84.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone.addChild(bone84);
		setRotationAngle(bone84, 0.0F, -1.0472F, 0.0F);
		

		bone85 = new RendererModel(this);
		bone85.setRotationPoint(0.0F, 3.0F, 40.5F);
		bone84.addChild(bone85);
		setRotationAngle(bone85, 0.2618F, 0.0F, 0.0F);
		bone85.cubeList.add(new ModelBox(bone85, 56, 34, -12.0F, -0.525F, -23.2F, 24, 1, 4, 0.0F, false));
		bone85.cubeList.add(new ModelBox(bone85, 56, 34, -14.0F, -0.525F, -19.2F, 28, 1, 4, 0.0F, false));
		bone85.cubeList.add(new ModelBox(bone85, 56, 34, -16.0F, -0.525F, -15.2F, 32, 1, 4, 0.0F, false));
		bone85.cubeList.add(new ModelBox(bone85, 56, 34, -18.0F, -0.525F, -11.2F, 36, 1, 4, 0.0F, false));
		bone85.cubeList.add(new ModelBox(bone85, 56, 34, -20.0F, -0.525F, -7.2F, 40, 1, 4, 0.0F, false));
		bone85.cubeList.add(new ModelBox(bone85, 56, 34, -22.0F, -0.525F, -3.2F, 44, 1, 2, 0.0F, false));
		bone85.cubeList.add(new ModelBox(bone85, 56, 34, -10.0F, -0.525F, -27.2F, 20, 1, 4, 0.0F, false));
		bone85.cubeList.add(new ModelBox(bone85, 56, 34, 6.0F, -1.525F, -25.2F, 1, 1, 3, 0.0F, false));
		bone85.cubeList.add(new ModelBox(bone85, 56, 34, -7.0F, -1.525F, -25.2F, 1, 1, 3, 0.0F, false));
		bone85.cubeList.add(new ModelBox(bone85, 56, 34, -6.0F, -1.525F, -26.2F, 12, 1, 1, 0.0F, false));
		bone85.cubeList.add(new ModelBox(bone85, 56, 34, -8.0F, -0.525F, -30.2F, 16, 1, 3, 0.0F, false));
		bone85.cubeList.add(new ModelBox(bone85, 56, 34, -6.0F, -0.525F, -32.2F, 12, 1, 2, 0.0F, false));

		bone86 = new RendererModel(this);
		bone86.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone84.addChild(bone86);
		setRotationAngle(bone86, 0.0F, -1.0472F, 0.0F);
		

		bone87 = new RendererModel(this);
		bone87.setRotationPoint(0.0F, 3.0F, 40.5F);
		bone86.addChild(bone87);
		setRotationAngle(bone87, 0.2618F, 0.0F, 0.0F);
		bone87.cubeList.add(new ModelBox(bone87, 56, 34, -12.0F, -0.525F, -23.2F, 24, 1, 4, 0.0F, false));
		bone87.cubeList.add(new ModelBox(bone87, 56, 34, -14.0F, -0.525F, -19.2F, 28, 1, 4, 0.0F, false));
		bone87.cubeList.add(new ModelBox(bone87, 56, 34, -16.0F, -0.525F, -15.2F, 32, 1, 4, 0.0F, false));
		bone87.cubeList.add(new ModelBox(bone87, 56, 34, -18.0F, -0.525F, -11.2F, 36, 1, 4, 0.0F, false));
		bone87.cubeList.add(new ModelBox(bone87, 56, 34, -20.0F, -0.525F, -7.2F, 40, 1, 4, 0.0F, false));
		bone87.cubeList.add(new ModelBox(bone87, 56, 34, -22.0F, -0.525F, -3.2F, 44, 1, 1, 0.0F, false));
		bone87.cubeList.add(new ModelBox(bone87, 56, 34, -10.0F, -0.525F, -27.2F, 20, 1, 4, 0.0F, false));
		bone87.cubeList.add(new ModelBox(bone87, 56, 34, 6.0F, -1.525F, -25.2F, 1, 1, 3, 0.0F, false));
		bone87.cubeList.add(new ModelBox(bone87, 56, 34, -7.0F, -1.525F, -25.2F, 1, 1, 3, 0.0F, false));
		bone87.cubeList.add(new ModelBox(bone87, 56, 34, -6.0F, -1.525F, -26.2F, 12, 1, 1, 0.0F, false));
		bone87.cubeList.add(new ModelBox(bone87, 56, 34, -8.0F, -0.525F, -30.2F, 16, 1, 3, 0.0F, false));
		bone87.cubeList.add(new ModelBox(bone87, 56, 34, -6.0F, -0.525F, -32.2F, 12, 1, 2, 0.0F, false));

		bone88 = new RendererModel(this);
		bone88.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone86.addChild(bone88);
		setRotationAngle(bone88, 0.0F, -1.0472F, 0.0F);
		

		bone89 = new RendererModel(this);
		bone89.setRotationPoint(0.0F, 3.0F, 40.5F);
		bone88.addChild(bone89);
		setRotationAngle(bone89, 0.2618F, 0.0F, 0.0F);
		bone89.cubeList.add(new ModelBox(bone89, 56, 34, -12.0F, -0.525F, -23.2F, 24, 1, 4, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 34, -14.0F, -0.525F, -19.2F, 28, 1, 4, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 34, -16.0F, -0.525F, -15.2F, 32, 1, 4, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 34, -18.0F, -0.525F, -11.2F, 36, 1, 4, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 34, -20.0F, -0.525F, -7.2F, 40, 1, 4, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 34, -22.0F, -0.525F, -3.2F, 44, 1, 2, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 34, -10.0F, -0.525F, -27.2F, 20, 1, 4, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 34, 6.0F, -1.525F, -25.2F, 1, 1, 3, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 34, -7.0F, -1.525F, -25.2F, 1, 1, 3, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 34, -6.0F, -1.525F, -26.2F, 12, 1, 1, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 34, -8.0F, -0.525F, -30.2F, 16, 1, 3, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 56, 34, -6.0F, -0.525F, -32.2F, 12, 1, 2, 0.0F, false));

		bone90 = new RendererModel(this);
		bone90.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone88.addChild(bone90);
		setRotationAngle(bone90, 0.0F, -1.0472F, 0.0F);
		

		bone91 = new RendererModel(this);
		bone91.setRotationPoint(0.0F, 3.0F, 40.5F);
		bone90.addChild(bone91);
		setRotationAngle(bone91, 0.2618F, 0.0F, 0.0F);
		bone91.cubeList.add(new ModelBox(bone91, 56, 34, -12.0F, -0.525F, -23.2F, 24, 1, 4, 0.0F, false));
		bone91.cubeList.add(new ModelBox(bone91, 56, 34, -14.0F, -0.525F, -19.2F, 28, 1, 4, 0.0F, false));
		bone91.cubeList.add(new ModelBox(bone91, 56, 34, -16.0F, -0.525F, -15.2F, 32, 1, 4, 0.0F, false));
		bone91.cubeList.add(new ModelBox(bone91, 56, 34, -18.0F, -0.525F, -11.2F, 36, 1, 4, 0.0F, false));
		bone91.cubeList.add(new ModelBox(bone91, 56, 34, -20.0F, -0.525F, -7.2F, 40, 1, 4, 0.0F, false));
		bone91.cubeList.add(new ModelBox(bone91, 56, 34, -22.0F, -0.525F, -3.2F, 44, 1, 2, 0.0F, false));
		bone91.cubeList.add(new ModelBox(bone91, 56, 34, -10.0F, -0.525F, -27.2F, 20, 1, 4, 0.0F, false));
		bone91.cubeList.add(new ModelBox(bone91, 56, 34, 6.0F, -1.525F, -25.2F, 1, 1, 3, 0.0F, false));
		bone91.cubeList.add(new ModelBox(bone91, 56, 34, -7.0F, -1.525F, -25.2F, 1, 1, 3, 0.0F, false));
		bone91.cubeList.add(new ModelBox(bone91, 56, 34, -6.0F, -1.525F, -26.2F, 12, 1, 1, 0.0F, false));
		bone91.cubeList.add(new ModelBox(bone91, 56, 34, -8.0F, -0.525F, -30.2F, 16, 1, 3, 0.0F, false));
		bone91.cubeList.add(new ModelBox(bone91, 56, 34, -6.0F, -0.525F, -32.2F, 12, 1, 2, 0.0F, false));

		panels = new RendererModel(this);
		panels.setRotationPoint(0.0F, 2.5F, 0.0F);
		

		bone13 = new RendererModel(this);
		bone13.setRotationPoint(0.0F, -3.0F, 40.5F);
		panels.addChild(bone13);
		setRotationAngle(bone13, -0.3491F, 0.0F, 0.0F);
		bone13.cubeList.add(new ModelBox(bone13, 57, 34, 6.0F, -0.475F, -23.2F, 6, 1, 4, 0.0F, false));
		bone13.cubeList.add(new ModelBox(bone13, 57, 34, -12.0F, -0.475F, -23.2F, 6, 1, 4, 0.0F, false));
		bone13.cubeList.add(new ModelBox(bone13, 56, 34, -14.0F, -0.475F, -19.2F, 28, 1, 4, 0.0F, false));
		bone13.cubeList.add(new ModelBox(bone13, 56, 34, -16.0F, -0.475F, -15.2F, 32, 1, 4, 0.0F, false));
		bone13.cubeList.add(new ModelBox(bone13, 56, 34, -18.0F, -0.475F, -11.2F, 36, 1, 4, 0.0F, false));
		bone13.cubeList.add(new ModelBox(bone13, 56, 34, -20.0F, -0.475F, -7.2F, 40, 1, 4, 0.0F, false));
		bone13.cubeList.add(new ModelBox(bone13, 56, 34, -22.0F, -0.475F, -3.2F, 44, 1, 4, 0.0F, false));
		bone13.cubeList.add(new ModelBox(bone13, 59, 34, 6.0F, -0.475F, -27.2F, 4, 2, 4, 0.0F, false));
		bone13.cubeList.add(new ModelBox(bone13, 61, 34, -6.0F, -0.475F, -27.2F, 12, 2, 2, 0.0F, false));
		bone13.cubeList.add(new ModelBox(bone13, 59, 34, -10.0F, -0.475F, -27.2F, 4, 2, 4, 0.0F, false));
		bone13.cubeList.add(new ModelBox(bone13, 56, 34, -9.0F, -0.475F, -29.2F, 18, 1, 2, 0.0F, false));

		bone50 = new RendererModel(this);
		bone50.setRotationPoint(3.0F, 0.0F, -21.2F);
		bone13.addChild(bone50);
		setRotationAngle(bone50, 0.2618F, 0.0F, 0.0F);
		bone50.cubeList.add(new ModelBox(bone50, 67, 46, -9.0F, 0.0597F, -4.9387F, 12, 1, 7, 0.0F, false));

		bone8 = new RendererModel(this);
		bone8.setRotationPoint(0.0F, 0.0F, 0.0F);
		panels.addChild(bone8);
		setRotationAngle(bone8, 0.0F, -1.0472F, 0.0F);
		

		bone14 = new RendererModel(this);
		bone14.setRotationPoint(0.0F, -3.0F, 40.5F);
		bone8.addChild(bone14);
		setRotationAngle(bone14, -0.3491F, 0.0F, 0.0F);
		bone14.cubeList.add(new ModelBox(bone14, 56, 34, -12.0F, -0.475F, -23.2F, 24, 1, 4, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 56, 34, -14.0F, -0.475F, -19.2F, 28, 1, 4, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 56, 34, -16.0F, -0.475F, -15.2F, 32, 1, 4, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 56, 34, -18.0F, -0.475F, -11.2F, 36, 1, 4, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 56, 34, -20.0F, -0.475F, -7.2F, 40, 1, 4, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 56, 34, -22.0F, -0.475F, -3.2F, 44, 1, 4, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 56, 34, -9.5F, -0.475F, -27.2F, 19, 1, 4, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 56, 34, -9.0F, -0.475F, -29.2F, 18, 1, 2, 0.0F, false));

		bone9 = new RendererModel(this);
		bone9.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone8.addChild(bone9);
		setRotationAngle(bone9, 0.0F, -1.0472F, 0.0F);
		

		bone15 = new RendererModel(this);
		bone15.setRotationPoint(0.0F, -3.0F, 40.5F);
		bone9.addChild(bone15);
		setRotationAngle(bone15, -0.3491F, 0.0F, 0.0F);
		bone15.cubeList.add(new ModelBox(bone15, 56, 34, -12.0F, -0.475F, -23.2F, 24, 1, 4, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 56, 34, -14.0F, -0.475F, -19.2F, 28, 1, 4, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 56, 34, -22.0F, -0.475F, -3.2F, 44, 1, 4, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 56, 34, -16.0F, -0.475F, -15.2F, 32, 1, 4, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 56, 34, -18.0F, -0.475F, -11.2F, 36, 1, 4, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 56, 34, -20.0F, -0.475F, -7.2F, 40, 1, 4, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 56, 34, -9.5F, -0.475F, -27.2F, 19, 1, 4, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 56, 34, -9.0F, -0.475F, -29.2F, 18, 1, 2, 0.0F, false));

		bone10 = new RendererModel(this);
		bone10.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone9.addChild(bone10);
		setRotationAngle(bone10, 0.0F, -1.0472F, 0.0F);
		

		bone16 = new RendererModel(this);
		bone16.setRotationPoint(0.0F, -3.0F, 40.5F);
		bone10.addChild(bone16);
		setRotationAngle(bone16, -0.3491F, 0.0F, 0.0F);
		bone16.cubeList.add(new ModelBox(bone16, 56, 34, 8.0F, -0.475F, -23.2F, 4, 3, 4, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 58, 34, -8.0F, -0.475F, -23.2F, 16, 3, 2, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 56, 34, -12.0F, -0.475F, -23.2F, 4, 3, 4, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 56, 34, 8.0F, -0.475F, -19.2F, 6, 3, 4, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 56, 34, -14.0F, -0.475F, -19.2F, 6, 3, 4, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 57, 34, 9.0F, -0.475F, -15.2F, 7, 1, 4, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 57, 34, -16.0F, -0.475F, -15.2F, 7, 1, 4, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 61, 34, 16.0F, -0.475F, -11.2F, 2, 1, 4, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 75, 47, 12.0F, 0.525F, -9.2F, 5, 1, 6, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 75, 47, -17.0F, 0.525F, -9.2F, 5, 1, 6, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 60, 34, -16.0F, -0.475F, -11.2F, 4, 1, 2, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 60, 34, 12.0F, -0.475F, -11.2F, 4, 1, 2, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 72, 34, -12.0F, -0.475F, -11.2F, 24, 1, 4, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 61, 34, -18.0F, -0.475F, -11.2F, 2, 1, 4, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 72, 34, -12.0F, -0.475F, -7.2F, 24, 1, 4, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 56, 34, -22.0F, -0.475F, -3.2F, 44, 1, 4, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 56, 34, -9.5F, -0.475F, -27.2F, 19, 1, 4, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 56, 34, -9.0F, -0.475F, -29.2F, 18, 1, 2, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 59, 34, -20.0F, -0.475F, -7.2F, 3, 1, 4, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 59, 34, 17.0F, -0.475F, -7.2F, 3, 1, 4, 0.0F, false));

		bone49 = new RendererModel(this);
		bone49.setRotationPoint(0.0F, 0.0F, -9.2F);
		bone16.addChild(bone49);
		setRotationAngle(bone49, 0.2618F, 0.0F, 0.0F);
		bone49.cubeList.add(new ModelBox(bone49, 57, 42, -9.0F, -0.9756F, -12.8024F, 18, 1, 11, 0.0F, false));

		bone11 = new RendererModel(this);
		bone11.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone10.addChild(bone11);
		setRotationAngle(bone11, 0.0F, -1.0472F, 0.0F);
		

		bone17 = new RendererModel(this);
		bone17.setRotationPoint(0.0F, -3.0F, 40.5F);
		bone11.addChild(bone17);
		setRotationAngle(bone17, -0.3491F, 0.0F, 0.0F);
		bone17.cubeList.add(new ModelBox(bone17, 56, 34, -22.0F, -0.475F, -3.2F, 44, 1, 4, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 56, 34, -20.0F, -0.475F, -7.2F, 40, 1, 4, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 56, 34, -18.0F, -0.475F, -11.2F, 36, 1, 4, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 56, 34, -16.0F, -0.475F, -15.2F, 32, 1, 4, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 56, 34, -14.0F, -0.475F, -19.2F, 28, 1, 4, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 56, 34, -12.0F, -0.475F, -23.2F, 24, 1, 4, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 56, 34, -9.5F, -0.475F, -27.2F, 19, 1, 4, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 56, 34, -9.0F, -0.475F, -29.2F, 18, 1, 2, 0.0F, false));

		bone12 = new RendererModel(this);
		bone12.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone11.addChild(bone12);
		setRotationAngle(bone12, 0.0F, -1.0472F, 0.0F);
		

		bone18 = new RendererModel(this);
		bone18.setRotationPoint(0.0F, -3.0F, 40.5F);
		bone12.addChild(bone18);
		setRotationAngle(bone18, -0.3491F, 0.0F, 0.0F);
		bone18.cubeList.add(new ModelBox(bone18, 56, 34, -22.0F, -0.475F, -3.2F, 44, 1, 4, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 56, 34, -14.0F, -0.475F, -19.2F, 28, 1, 4, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 56, 34, -12.0F, -0.475F, -23.2F, 24, 1, 4, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 58, 34, -6.5F, -0.475F, -7.2F, 13, 1, 4, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 58, 34, -6.5F, -0.475F, -11.2F, 13, 1, 4, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 58, 34, -6.5F, -0.475F, -15.2F, 13, 1, 4, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 66, 41, -14.5F, 0.025F, -15.2F, 8, 1, 12, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 66, 41, 6.5F, 0.025F, -15.2F, 8, 1, 12, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 64, 34, -16.0F, -0.475F, -15.2F, 4, 1, 4, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 64, 34, 12.0F, -0.475F, -15.2F, 4, 1, 4, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 64, 34, -18.0F, -0.475F, -11.2F, 4, 1, 4, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 64, 34, 14.0F, -0.475F, -11.2F, 4, 1, 4, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 61, 34, -20.0F, -0.475F, -7.2F, 6, 1, 4, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 61, 34, 14.0F, -0.475F, -7.2F, 6, 1, 4, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 56, 34, -9.5F, -0.475F, -27.2F, 19, 1, 4, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 56, 34, -9.0F, -0.475F, -29.2F, 18, 1, 2, 0.0F, false));

		bone64 = new RendererModel(this);
		bone64.setRotationPoint(7.0F, -1.0F, -17.2F);
		bone18.addChild(bone64);
		setRotationAngle(bone64, 0.2618F, 0.0F, 0.0F);
		bone64.cubeList.add(new ModelBox(bone64, 76, 47, -2.0F, -0.975F, -3.875F, 5, 2, 5, 0.0F, false));

		topedges = new RendererModel(this);
		topedges.setRotationPoint(0.0F, -0.25F, 0.0F);
		

		bone20 = new RendererModel(this);
		bone20.setRotationPoint(0.0F, -3.0F, 40.5F);
		topedges.addChild(bone20);
		setRotationAngle(bone20, -0.2618F, 0.0F, 0.0F);
		bone20.cubeList.add(new ModelBox(bone20, 85, 81, -6.5F, -0.8159F, -29.7088F, 13, 2, 2, 0.0F, false));

		bone21 = new RendererModel(this);
		bone21.setRotationPoint(0.0F, 0.0F, 0.0F);
		topedges.addChild(bone21);
		setRotationAngle(bone21, 0.0F, -1.0472F, 0.0F);
		

		bone22 = new RendererModel(this);
		bone22.setRotationPoint(0.0F, -3.0F, 40.5F);
		bone21.addChild(bone22);
		setRotationAngle(bone22, -0.2618F, 0.0F, 0.0F);
		bone22.cubeList.add(new ModelBox(bone22, 85, 81, -6.5F, -0.8159F, -29.7088F, 13, 2, 2, 0.0F, false));

		bone23 = new RendererModel(this);
		bone23.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone21.addChild(bone23);
		setRotationAngle(bone23, 0.0F, -1.0472F, 0.0F);
		

		bone24 = new RendererModel(this);
		bone24.setRotationPoint(0.0F, -3.0F, 40.5F);
		bone23.addChild(bone24);
		setRotationAngle(bone24, -0.2618F, 0.0F, 0.0F);
		bone24.cubeList.add(new ModelBox(bone24, 85, 81, -6.5F, -0.8159F, -29.7088F, 13, 2, 2, 0.0F, false));

		bone25 = new RendererModel(this);
		bone25.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone23.addChild(bone25);
		setRotationAngle(bone25, 0.0F, -1.0472F, 0.0F);
		

		bone26 = new RendererModel(this);
		bone26.setRotationPoint(0.0F, -3.0F, 40.5F);
		bone25.addChild(bone26);
		setRotationAngle(bone26, -0.2618F, 0.0F, 0.0F);
		bone26.cubeList.add(new ModelBox(bone26, 85, 81, -6.5F, -0.8159F, -29.7088F, 13, 2, 2, 0.0F, false));

		bone27 = new RendererModel(this);
		bone27.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone25.addChild(bone27);
		setRotationAngle(bone27, 0.0F, -1.0472F, 0.0F);
		

		bone28 = new RendererModel(this);
		bone28.setRotationPoint(0.0F, -3.0F, 40.5F);
		bone27.addChild(bone28);
		setRotationAngle(bone28, -0.2618F, 0.0F, 0.0F);
		bone28.cubeList.add(new ModelBox(bone28, 85, 81, -6.5F, -0.8159F, -29.7088F, 13, 2, 2, 0.0F, false));

		bone29 = new RendererModel(this);
		bone29.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone27.addChild(bone29);
		setRotationAngle(bone29, 0.0F, -1.0472F, 0.0F);
		

		bone30 = new RendererModel(this);
		bone30.setRotationPoint(0.0F, -3.0F, 40.5F);
		bone29.addChild(bone30);
		setRotationAngle(bone30, -0.2618F, 0.0F, 0.0F);
		bone30.cubeList.add(new ModelBox(bone30, 85, 81, -6.5F, -0.8159F, -29.7088F, 13, 2, 2, 0.0F, false));

		centre = new RendererModel(this);
		centre.setRotationPoint(0.0F, -7.0F, 0.0F);
		centre.cubeList.add(new ModelBox(centre, 59, 57, -6.0F, -6.5F, -2.6F, 12, 1, 13, 0.0F, false));
		centre.cubeList.add(new ModelBox(centre, 56, 56, -7.5F, -62.5F, 0.1F, 15, 2, 13, 0.0F, false));
		centre.cubeList.add(new ModelBox(centre, 58, 56, -6.5F, -60.5F, -1.65F, 13, 2, 13, 0.0F, false));
		centre.cubeList.add(new ModelBox(centre, 57, 56, -7.0F, -5.5F, -0.85F, 14, 2, 13, 0.0F, false));

		bone66 = new RendererModel(this);
		bone66.setRotationPoint(0.0F, 0.0F, 0.0F);
		centre.addChild(bone66);
		setRotationAngle(bone66, 0.0F, -1.0472F, 0.0F);
		bone66.cubeList.add(new ModelBox(bone66, 59, 57, -6.0F, -6.5F, -2.6F, 12, 1, 13, 0.0F, false));
		bone66.cubeList.add(new ModelBox(bone66, 56, 56, -7.5F, -62.5F, 0.1F, 15, 2, 13, 0.0F, false));
		bone66.cubeList.add(new ModelBox(bone66, 58, 56, -6.5F, -60.5F, -1.65F, 13, 2, 13, 0.0F, false));
		bone66.cubeList.add(new ModelBox(bone66, 57, 56, -7.0F, -5.5F, -0.85F, 14, 2, 13, 0.0F, false));

		bone67 = new RendererModel(this);
		bone67.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone66.addChild(bone67);
		setRotationAngle(bone67, 0.0F, -1.0472F, 0.0F);
		bone67.cubeList.add(new ModelBox(bone67, 59, 57, -6.0F, -6.5F, -2.6F, 12, 1, 13, 0.0F, false));
		bone67.cubeList.add(new ModelBox(bone67, 56, 56, -7.5F, -62.5F, 0.1F, 15, 2, 13, 0.0F, false));
		bone67.cubeList.add(new ModelBox(bone67, 58, 56, -6.5F, -60.5F, -1.65F, 13, 2, 13, 0.0F, false));
		bone67.cubeList.add(new ModelBox(bone67, 57, 56, -7.0F, -5.5F, -0.85F, 14, 2, 13, 0.0F, false));

		bone68 = new RendererModel(this);
		bone68.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone67.addChild(bone68);
		setRotationAngle(bone68, 0.0F, -1.0472F, 0.0F);
		bone68.cubeList.add(new ModelBox(bone68, 59, 57, -6.0F, -6.5F, -2.6F, 12, 1, 13, 0.0F, false));
		bone68.cubeList.add(new ModelBox(bone68, 56, 56, -7.5F, -62.5F, 0.1F, 15, 2, 13, 0.0F, false));
		bone68.cubeList.add(new ModelBox(bone68, 58, 56, -6.5F, -60.5F, -1.65F, 13, 2, 13, 0.0F, false));
		bone68.cubeList.add(new ModelBox(bone68, 57, 56, -7.0F, -5.5F, -0.85F, 14, 2, 13, 0.0F, false));

		bone69 = new RendererModel(this);
		bone69.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone68.addChild(bone69);
		setRotationAngle(bone69, 0.0F, -1.0472F, 0.0F);
		bone69.cubeList.add(new ModelBox(bone69, 59, 57, -6.0F, -6.5F, -2.6F, 12, 1, 13, 0.0F, false));
		bone69.cubeList.add(new ModelBox(bone69, 56, 56, -7.5F, -62.5F, 0.1F, 15, 2, 13, 0.0F, false));
		bone69.cubeList.add(new ModelBox(bone69, 58, 56, -6.5F, -60.5F, -1.65F, 13, 2, 13, 0.0F, false));
		bone69.cubeList.add(new ModelBox(bone69, 57, 56, -7.0F, -5.5F, -0.85F, 14, 2, 13, 0.0F, false));

		bone70 = new RendererModel(this);
		bone70.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone69.addChild(bone70);
		setRotationAngle(bone70, 0.0F, -1.0472F, 0.0F);
		bone70.cubeList.add(new ModelBox(bone70, 59, 57, -6.0F, -6.5F, -2.6F, 12, 1, 13, 0.0F, false));
		bone70.cubeList.add(new ModelBox(bone70, 56, 56, -7.5F, -62.5F, 0.1F, 15, 2, 13, 0.0F, false));
		bone70.cubeList.add(new ModelBox(bone70, 58, 56, -6.5F, -60.5F, -1.65F, 13, 2, 13, 0.0F, false));
		bone70.cubeList.add(new ModelBox(bone70, 57, 56, -7.0F, -5.5F, -0.85F, 14, 2, 13, 0.0F, false));

		base = new RendererModel(this);
		base.setRotationPoint(0.0F, 16.0F, 0.0F);
		base.cubeList.add(new ModelBox(base, 59, 57, -6.0F, -6.5F, -2.6F, 12, 1, 13, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 59, 56, -6.0F, 5.475F, -2.6F, 12, 3, 13, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 54, 57, -8.5F, 7.5F, 1.75F, 17, 1, 13, 0.0F, false));

		bone97 = new RendererModel(this);
		bone97.setRotationPoint(0.0F, 0.0F, 0.0F);
		base.addChild(bone97);
		setRotationAngle(bone97, 0.0F, -1.0472F, 0.0F);
		bone97.cubeList.add(new ModelBox(bone97, 59, 57, -6.0F, -6.5F, -2.6F, 12, 1, 13, 0.0F, false));
		bone97.cubeList.add(new ModelBox(bone97, 59, 56, -6.0F, 5.475F, -2.6F, 12, 3, 13, 0.0F, false));
		bone97.cubeList.add(new ModelBox(bone97, 54, 57, -8.5F, 7.5F, 1.75F, 17, 1, 13, 0.0F, false));

		bone98 = new RendererModel(this);
		bone98.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone97.addChild(bone98);
		setRotationAngle(bone98, 0.0F, -1.0472F, 0.0F);
		bone98.cubeList.add(new ModelBox(bone98, 59, 57, -6.0F, -6.5F, -2.6F, 12, 1, 13, 0.0F, false));
		bone98.cubeList.add(new ModelBox(bone98, 59, 56, -6.0F, 5.475F, -2.6F, 12, 3, 13, 0.0F, false));
		bone98.cubeList.add(new ModelBox(bone98, 54, 57, -8.5F, 7.5F, 1.75F, 17, 1, 13, 0.0F, false));

		bone99 = new RendererModel(this);
		bone99.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone98.addChild(bone99);
		setRotationAngle(bone99, 0.0F, -1.0472F, 0.0F);
		bone99.cubeList.add(new ModelBox(bone99, 59, 57, -6.0F, -6.5F, -2.6F, 12, 1, 13, 0.0F, false));
		bone99.cubeList.add(new ModelBox(bone99, 59, 56, -6.0F, 5.475F, -2.6F, 12, 3, 13, 0.0F, false));
		bone99.cubeList.add(new ModelBox(bone99, 54, 57, -8.5F, 7.5F, 1.75F, 17, 1, 13, 0.0F, false));

		bone100 = new RendererModel(this);
		bone100.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone99.addChild(bone100);
		setRotationAngle(bone100, 0.0F, -1.0472F, 0.0F);
		bone100.cubeList.add(new ModelBox(bone100, 59, 57, -6.0F, -6.5F, -2.6F, 12, 1, 13, 0.0F, false));
		bone100.cubeList.add(new ModelBox(bone100, 59, 56, -6.0F, 5.475F, -2.6F, 12, 3, 13, 0.0F, false));
		bone100.cubeList.add(new ModelBox(bone100, 54, 57, -8.5F, 7.5F, 1.75F, 17, 1, 13, 0.0F, false));

		bone101 = new RendererModel(this);
		bone101.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone100.addChild(bone101);
		setRotationAngle(bone101, 0.0F, -1.0472F, 0.0F);
		bone101.cubeList.add(new ModelBox(bone101, 59, 57, -6.0F, -6.5F, -2.6F, 12, 1, 13, 0.0F, false));
		bone101.cubeList.add(new ModelBox(bone101, 59, 56, -6.0F, 5.475F, -2.6F, 12, 3, 13, 0.0F, false));
		bone101.cubeList.add(new ModelBox(bone101, 54, 57, -8.5F, 7.5F, 1.75F, 17, 1, 13, 0.0F, false));

		NorthWest = new RendererModel(this);
		NorthWest.setRotationPoint(0.0F, 3.0F, 0.0F);
		setRotationAngle(NorthWest, 0.0F, -1.0472F, 0.0F);
		

		rotation7 = new RendererModel(this);
		rotation7.setRotationPoint(0.0F, 0.0F, 0.0F);
		NorthWest.addChild(rotation7);
		setRotationAngle(rotation7, 0.3491F, 0.0F, 0.0F);
		rotation7.cubeList.add(new ModelBox(rotation7, 69, 22, 14.0F, -18.8402F, -32.5119F, 1, 2, 1, 0.0F, false));
		rotation7.cubeList.add(new ModelBox(rotation7, 104, 26, 4.0F, -18.0902F, -19.5119F, 3, 1, 3, 0.0F, false));
		rotation7.cubeList.add(new ModelBox(rotation7, 95, 26, -6.0F, -18.3402F, -30.5119F, 2, 1, 2, 0.0F, false));
		rotation7.cubeList.add(new ModelBox(rotation7, 95, 26, 0.0F, -18.5902F, -32.5119F, 2, 1, 2, 0.0F, false));
		rotation7.cubeList.add(new ModelBox(rotation7, 98, 17, -3.0F, -18.0902F, -25.5119F, 5, 1, 5, 0.0F, false));
		rotation7.cubeList.add(new ModelBox(rotation7, 95, 26, -2.0F, -18.0902F, -16.5119F, 2, 1, 2, 0.0F, false));
		rotation7.cubeList.add(new ModelBox(rotation7, 104, 26, -6.25F, -18.0902F, -20.0119F, 3, 1, 3, 0.0F, false));

		Dimensioncontrol = new RendererModel(this);
		Dimensioncontrol.setRotationPoint(0.0F, 0.8966F, -4.3813F);
		rotation7.addChild(Dimensioncontrol);
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 101, 21, 3.0F, -18.9868F, -28.6305F, 3, 1, 1, 0.0F, false));
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 101, 21, 6.0F, -18.9868F, -27.6305F, 2, 1, 1, 0.0F, false));
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 101, 21, 7.5F, -18.9868F, -27.1305F, 1, 1, 2, 0.0F, false));
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 104, 19, 8.5F, -18.9868F, -25.1305F, 1, 1, 3, 0.0F, false));
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 101, 16, 9.5F, -18.9868F, -22.1305F, 1, 1, 6, 0.0F, false));
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 104, 19, 8.5F, -18.9868F, -16.1305F, 1, 1, 3, 0.0F, false));
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 101, 21, 7.5F, -18.9868F, -13.1305F, 1, 1, 2, 0.0F, false));
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 101, 21, 6.0F, -18.9868F, -11.6305F, 2, 1, 1, 0.0F, false));
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 101, 21, 3.0F, -18.9868F, -10.6305F, 3, 1, 1, 0.0F, false));
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 101, 21, -3.0F, -18.9868F, -9.6305F, 6, 1, 1, 0.0F, false));
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 101, 21, -6.0F, -18.9868F, -10.6305F, 3, 1, 1, 0.0F, false));
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 101, 21, -8.0F, -18.9868F, -11.6305F, 2, 1, 1, 0.0F, false));
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 101, 21, -8.5F, -18.9868F, -13.1305F, 1, 1, 2, 0.0F, false));
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 104, 19, -9.5F, -18.9868F, -16.1305F, 1, 1, 3, 0.0F, false));
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 101, 16, -10.5F, -18.9868F, -22.1305F, 1, 1, 6, 0.0F, false));
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 104, 19, -9.5F, -18.9868F, -25.1305F, 1, 1, 3, 0.0F, false));
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 101, 21, -8.5F, -18.9868F, -27.1305F, 1, 1, 2, 0.0F, false));
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 101, 21, -8.0F, -18.9868F, -27.6305F, 2, 1, 1, 0.0F, false));
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 101, 21, -6.0F, -18.9868F, -28.6305F, 3, 1, 1, 0.0F, false));
		Dimensioncontrol.cubeList.add(new ModelBox(Dimensioncontrol, 101, 21, -3.0F, -18.9868F, -29.6305F, 6, 1, 1, 0.0F, false));

		bone108 = new RendererModel(this);
		bone108.setRotationPoint(-12.5F, -18.3402F, -28.0119F);
		rotation7.addChild(bone108);
		setRotationAngle(bone108, 0.0F, 0.6981F, 0.0F);
		bone108.cubeList.add(new ModelBox(bone108, 88, 22, -0.5F, 0.0F, -0.5F, 1, 1, 1, 0.0F, false));
		bone108.cubeList.add(new ModelBox(bone108, 86, 25, -1.5F, -1.0F, -0.5F, 3, 1, 1, 0.0F, false));

		fastreturn = new RendererModel(this);
		fastreturn.setRotationPoint(-21.0F, 1.3966F, -5.3813F);
		rotation7.addChild(fastreturn);
		fastreturn.cubeList.add(new ModelBox(fastreturn, 110, 3, 5.0F, -19.7368F, -28.1305F, 3, 1, 3, 0.0F, false));

		bone107 = new RendererModel(this);
		bone107.setRotationPoint(5.5F, -18.3402F, -12.0119F);
		rotation7.addChild(bone107);
		setRotationAngle(bone107, 0.0F, 0.6981F, 0.0F);
		bone107.cubeList.add(new ModelBox(bone107, 79, 24, -0.5F, 0.0F, -0.5F, 1, 1, 1, 0.0F, false));
		bone107.cubeList.add(new ModelBox(bone107, 76, 22, -2.5F, -1.0F, -0.5F, 4, 1, 1, 0.0F, false));

		bone46 = new RendererModel(this);
		bone46.setRotationPoint(4.5F, -18.3402F, -29.0119F);
		rotation7.addChild(bone46);
		setRotationAngle(bone46, 0.0F, -2.618F, 0.0F);
		bone46.cubeList.add(new ModelBox(bone46, 104, 26, -0.5F, 0.25F, -1.5F, 3, 1, 3, 0.0F, false));
		bone46.cubeList.add(new ModelBox(bone46, 100, 24, -3.5F, -0.75F, -0.5F, 7, 1, 1, 0.0F, false));

		bone47 = new RendererModel(this);
		bone47.setRotationPoint(-5.5F, -18.3402F, -19.0119F);
		rotation7.addChild(bone47);
		setRotationAngle(bone47, 0.0F, -0.7854F, 0.0F);
		bone47.cubeList.add(new ModelBox(bone47, 79, 24, 0.5F, 0.0F, -0.5F, 1, 1, 1, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 100, 24, -1.5F, -1.0F, -0.5F, 4, 1, 1, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 117, 24, -2.5F, -1.5F, -0.5F, 1, 1, 1, 0.0F, false));

		bone103 = new RendererModel(this);
		bone103.setRotationPoint(-3.5F, -18.8402F, -26.0119F);
		rotation7.addChild(bone103);
		setRotationAngle(bone103, 0.0F, 2.1817F, 0.0F);
		bone103.cubeList.add(new ModelBox(bone103, 117, 24, -11.5F, 0.0F, 6.5F, 1, 1, 1, 0.0F, false));
		bone103.cubeList.add(new ModelBox(bone103, 100, 24, -13.5F, -1.0F, 6.5F, 5, 1, 1, 0.0F, false));

		facing_rotate_x = new RendererModel(this);
		facing_rotate_x.setRotationPoint(14.7107F, -19.8045F, -31.8011F);
		rotation7.addChild(facing_rotate_x);
		facing_rotate_x.cubeList.add(new ModelBox(facing_rotate_x, 59, 24, -1.7107F, -0.5357F, -2.2107F, 3, 1, 1, 0.0F, false));
		facing_rotate_x.cubeList.add(new ModelBox(facing_rotate_x, 100, 24, -1.7107F, -0.0357F, -0.7107F, 3, 1, 1, 0.0F, false));
		facing_rotate_x.cubeList.add(new ModelBox(facing_rotate_x, 69, 25, -0.7107F, -0.2857F, -0.7107F, 1, 1, 1, 0.0F, false));
		facing_rotate_x.cubeList.add(new ModelBox(facing_rotate_x, 59, 22, 0.7893F, -0.5357F, -1.7107F, 1, 1, 3, 0.0F, false));
		facing_rotate_x.cubeList.add(new ModelBox(facing_rotate_x, 59, 24, -1.7107F, -0.5357F, 0.7893F, 3, 1, 1, 0.0F, false));
		facing_rotate_x.cubeList.add(new ModelBox(facing_rotate_x, 59, 22, -2.2107F, -0.5357F, -1.7107F, 1, 1, 3, 0.0F, false));
		facing_rotate_x.cubeList.add(new ModelBox(facing_rotate_x, 69, 25, 0.7643F, -1.0357F, 0.7643F, 1, 1, 1, 0.0F, false));

		NorthEast = new RendererModel(this);
		NorthEast.setRotationPoint(0.0F, 3.0F, 0.0F);
		setRotationAngle(NorthEast, 0.0F, 1.0472F, 0.0F);
		

		rotation3 = new RendererModel(this);
		rotation3.setRotationPoint(0.0F, 0.0F, 0.0F);
		NorthEast.addChild(rotation3);
		setRotationAngle(rotation3, 0.3491F, 0.0F, 0.0F);
		rotation3.cubeList.add(new ModelBox(rotation3, 0, 45, -7.25F, -18.0902F, -14.5119F, 2, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 9, 45, -7.15F, -18.5902F, -14.4119F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 9, 45, 6.1F, -18.5902F, -13.6619F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 26, 45, -1.5F, -17.8402F, -15.0119F, 3, 1, 3, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 14, 45, -3.5F, -17.8402F, -14.5119F, 2, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 14, 45, 1.5F, -17.8402F, -14.5119F, 2, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 0, 45, 5.25F, -18.0902F, -14.5119F, 2, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 0, 41, -15.0F, -18.3402F, -29.5119F, 2, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 27, 40, -17.0F, -18.3402F, -34.5119F, 3, 1, 3, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 9, 41, -12.0F, -18.0902F, -32.5119F, 2, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 18, 41, -8.0F, -18.0902F, -33.5119F, 2, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 0, 41, 13.0F, -18.3402F, -29.5119F, 2, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 9, 41, 10.0F, -18.0902F, -32.5119F, 2, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 18, 41, 6.0F, -18.0902F, -33.5119F, 2, 1, 2, 0.0F, false));

		refueller = new RendererModel(this);
		refueller.setRotationPoint(0.0F, 0.8966F, -4.3813F);
		rotation3.addChild(refueller);
		refueller.cubeList.add(new ModelBox(refueller, 0, 49, -1.0F, -19.4868F, -10.1305F, 2, 1, 2, 0.0F, false));

		bone48 = new RendererModel(this);
		bone48.setRotationPoint(0.0F, 0.8966F, -4.3813F);
		rotation3.addChild(bone48);
		bone48.cubeList.add(new ModelBox(bone48, 10, 50, -4.1F, -18.7368F, -13.1305F, 8, 1, 1, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 10, 50, -7.1F, -18.7368F, -14.1305F, 3, 1, 1, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 10, 50, 3.9F, -18.7368F, -14.1305F, 3, 1, 1, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 10, 50, -4.1F, -18.7368F, -26.1305F, 8, 1, 1, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 10, 50, -7.1F, -18.7368F, -25.1305F, 3, 1, 1, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 10, 50, 3.9F, -18.7368F, -25.1305F, 3, 1, 1, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 10, 48, 9.9F, -18.7368F, -21.1305F, 1, 1, 4, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 10, 50, -9.1F, -18.7368F, -24.1305F, 2, 1, 1, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 10, 50, 6.9F, -18.7368F, -24.1305F, 2, 1, 1, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 10, 50, -9.1F, -18.7368F, -15.1305F, 2, 1, 1, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 10, 50, 6.9F, -18.7368F, -15.1305F, 2, 1, 1, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 10, 50, -10.1F, -18.7368F, -17.1305F, 1, 1, 2, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 10, 50, 8.9F, -18.7368F, -17.1305F, 1, 1, 2, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 10, 50, -10.1F, -18.7368F, -23.1305F, 1, 1, 2, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 10, 50, 8.9F, -18.7368F, -23.1305F, 1, 1, 2, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 10, 48, -11.0F, -18.7368F, -21.1305F, 1, 1, 4, 0.0F, false));

		randomiser = new RendererModel(this);
		randomiser.setRotationPoint(0.0F, 0.8966F, -4.3813F);
		rotation3.addChild(randomiser);
		randomiser.cubeList.add(new ModelBox(randomiser, 27, 40, 14.0F, -19.2368F, -30.1305F, 3, 1, 3, 0.0F, false));

		South = new RendererModel(this);
		South.setRotationPoint(0.0F, 3.0F, 0.0F);
		setRotationAngle(South, 0.0F, 3.1416F, 0.0F);
		

		rotation5 = new RendererModel(this);
		rotation5.setRotationPoint(0.0F, 0.0F, 0.0F);
		South.addChild(rotation5);
		setRotationAngle(rotation5, 0.3491F, 0.0F, 0.0F);
		rotation5.cubeList.add(new ModelBox(rotation5, 0, 82, -15.375F, -17.9868F, -34.8805F, 2, 1, 8, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 28, 82, -14.875F, -18.2368F, -29.1305F, 1, 1, 1, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 0, 85, -13.375F, -17.9868F, -32.8805F, 5, 1, 5, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 15, 75, -5.5F, -17.7868F, -32.8805F, 11, 1, 5, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 22, 91, 7.0F, -18.2868F, -32.8805F, 7, 1, 5, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 23, 92, 7.0F, -19.2868F, -32.3805F, 7, 1, 4, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 11, 75, -7.5F, -17.7868F, -26.8805F, 15, 1, 5, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 0, 92, -6.5F, -18.2868F, -24.8805F, 4, 1, 2, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 19, 90, -1.75F, -18.5368F, -26.1305F, 1, 1, 3, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 19, 90, 0.75F, -18.5368F, -26.1305F, 1, 1, 3, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 0, 92, 2.5F, -18.2868F, -24.8805F, 4, 1, 2, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 0, 76, -5.0F, -18.2868F, -30.3805F, 2, 1, 2, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 9, 76, -4.5F, -18.0368F, -32.3805F, 1, 1, 2, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 9, 76, -1.75F, -18.0368F, -32.3805F, 1, 1, 2, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 9, 76, 0.75F, -18.0368F, -32.3805F, 1, 1, 2, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 9, 76, 3.5F, -18.0368F, -32.3805F, 1, 1, 2, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 0, 76, 0.25F, -18.2868F, -30.3805F, 2, 1, 2, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 0, 76, -2.25F, -18.2868F, -30.3805F, 2, 1, 2, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 0, 76, 3.0F, -18.2868F, -30.3805F, 2, 1, 2, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 2, 88, -13.375F, -17.9868F, -34.8805F, 6, 1, 2, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 16, 82, -9.625F, -18.2368F, -34.3805F, 1, 1, 1, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 20, 82, -10.125F, -18.2368F, -31.6305F, 1, 1, 1, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 24, 82, -12.125F, -18.2368F, -29.6305F, 1, 1, 1, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 0, 96, -11.75F, -17.9652F, -23.0119F, 3, 1, 3, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 34, 82, -1.75F, -17.8402F, -11.5119F, 1, 1, 1, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 34, 82, -4.25F, -17.8402F, -11.5119F, 1, 1, 1, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 34, 82, 1.0F, -17.8402F, -11.5119F, 1, 1, 1, 0.0F, false));
		rotation5.cubeList.add(new ModelBox(rotation5, 34, 82, 3.5F, -17.8402F, -11.5119F, 1, 1, 1, 0.0F, false));

		throttle_rotate_x = new RendererModel(this);
		throttle_rotate_x.setRotationPoint(10.5F, -18.0703F, -30.2555F);
		rotation5.addChild(throttle_rotate_x);
		setRotationAngle(throttle_rotate_x, -0.8727F, 0.0F, 0.0F);
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 42, 82, 1.5F, -3.0F, -1.0F, 1, 3, 2, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 42, 82, -2.5F, -3.0F, -1.0F, 1, 3, 2, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 42, 92, -2.5F, -5.0F, -0.5F, 1, 2, 1, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 42, 92, 1.5F, -5.0F, -0.5F, 1, 2, 1, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 34, 98, -3.5F, -6.0F, -0.5F, 7, 1, 1, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 37, 101, -2.5F, -6.25F, -0.525F, 5, 1, 1, 0.0F, false));

		handbrake_rotate_y = new RendererModel(this);
		handbrake_rotate_y.setRotationPoint(-13.125F, -14.6979F, -34.0776F);
		rotation5.addChild(handbrake_rotate_y);
		setRotationAngle(handbrake_rotate_y, 0.0F, 0.3491F, 0.0F);
		handbrake_rotate_y.cubeList.add(new ModelBox(handbrake_rotate_y, 30, 88, 4.9601F, -5.5655F, 0.5676F, 2, 1, 1, 0.0F, false));
		handbrake_rotate_y.cubeList.add(new ModelBox(handbrake_rotate_y, 17, 85, -1.9094F, -4.2889F, -0.3751F, 3, 1, 3, 0.0F, false));

		bone55 = new RendererModel(this);
		bone55.setRotationPoint(-0.5205F, -0.7575F, -0.0573F);
		handbrake_rotate_y.addChild(bone55);
		setRotationAngle(bone55, 0.0F, 0.0F, -0.0873F);
		bone55.cubeList.add(new ModelBox(bone55, 27, 85, -0.1212F, -4.3121F, 0.6249F, 6, 1, 1, 0.0F, false));

		stabiliser_translate_y = new RendererModel(this);
		stabiliser_translate_y.setRotationPoint(0.0F, 0.6617F, -4.2958F);
		rotation5.addChild(stabiliser_translate_y);
		stabiliser_translate_y.cubeList.add(new ModelBox(stabiliser_translate_y, 13, 94, -11.25F, -19.3618F, -17.6305F, 2, 1, 1, 0.0F, false));

		bone54 = new RendererModel(this);
		bone54.setRotationPoint(0.0F, -14.5513F, -15.7089F);
		rotation5.addChild(bone54);
		setRotationAngle(bone54, -0.2618F, 0.0F, 0.0F);
		bone54.cubeList.add(new ModelBox(bone54, 10, 98, -5.25F, -2.4867F, -1.195F, 10, 1, 3, 0.0F, false));

		ycontrol = new RendererModel(this);
		ycontrol.setRotationPoint(-1.25F, -1.9867F, 0.305F);
		bone54.addChild(ycontrol);
		ycontrol.cubeList.add(new ModelBox(ycontrol, 42, 87, -0.5F, -3.0F, -0.5F, 1, 3, 1, 0.0F, false));

		xcontrol = new RendererModel(this);
		xcontrol.setRotationPoint(-3.75F, -1.9867F, 0.305F);
		bone54.addChild(xcontrol);
		xcontrol.cubeList.add(new ModelBox(xcontrol, 42, 87, -0.5F, -3.0F, -0.5F, 1, 3, 1, 0.0F, false));

		zcontrol = new RendererModel(this);
		zcontrol.setRotationPoint(1.25F, -2.4867F, 0.305F);
		bone54.addChild(zcontrol);
		zcontrol.cubeList.add(new ModelBox(zcontrol, 42, 87, -0.5F, -2.5F, -0.5F, 1, 3, 1, 0.0F, false));

		bone95 = new RendererModel(this);
		bone95.setRotationPoint(3.75F, -1.9867F, 0.305F);
		bone54.addChild(bone95);
		bone95.cubeList.add(new ModelBox(bone95, 42, 87, -0.5F, -3.0F, -0.5F, 1, 3, 1, 0.0F, false));

		SouthWest = new RendererModel(this);
		SouthWest.setRotationPoint(0.0F, 3.0F, 0.0F);
		setRotationAngle(SouthWest, 0.0F, -2.0944F, 0.0F);
		

		rotation6 = new RendererModel(this);
		rotation6.setRotationPoint(0.0F, 0.0F, 0.0F);
		SouthWest.addChild(rotation6);
		setRotationAngle(rotation6, 0.3491F, 0.0F, 0.0F);
		rotation6.cubeList.add(new ModelBox(rotation6, 26, 106, -6.0F, -17.8402F, -32.5119F, 7, 1, 7, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 48, 120, -6.0F, -17.6902F, -12.5119F, 12, 1, 3, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 51, 110, -5.0F, -18.1902F, -11.5119F, 3, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 20, 121, -4.0F, -18.1902F, -16.5119F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 5, 119, -4.0F, -17.6902F, -22.0119F, 1, 1, 5, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 24, 121, -1.0F, -18.1902F, -16.5119F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 5, 119, -1.0F, -17.6902F, -22.0119F, 1, 1, 5, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 20, 121, 2.0F, -18.1902F, -16.5119F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 43, 115, 7.0F, -17.3657F, -33.1605F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 43, 117, 10.0F, -17.6157F, -33.1605F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 7, 115, -8.5F, -17.6157F, -33.1605F, 1, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 115, -10.25F, -17.6157F, -33.1605F, 1, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 15, 115, -11.5F, -17.3657F, -30.1605F, 4, 1, 4, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 28, 115, -11.75F, -17.6157F, -24.1605F, 5, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 115, -12.0F, -17.6157F, -33.1605F, 1, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 14, 115, -13.75F, -17.6157F, -33.4105F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 43, 117, 12.0F, -17.6157F, -33.1605F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 50, 115, 7.5F, -17.8657F, -23.6605F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 119, 9.5F, -17.3657F, -24.6605F, 2, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 18, 108, 8.0F, -17.6157F, -29.6605F, 3, 1, 3, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 52, 115, 7.5F, -18.8657F, -24.6605F, 1, 1, 3, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 32, 119, 12.5F, -17.3657F, -27.1605F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 32, 119, 12.5F, -17.3657F, -28.6605F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 32, 119, 12.5F, -17.3657F, -30.1605F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 5, 119, 2.0F, -17.6902F, -22.0119F, 1, 1, 5, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 25, 122, 5.5F, -17.9402F, -19.5119F, 4, 1, 4, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 38, 121, 6.0F, -18.1902F, -19.0119F, 3, 1, 3, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 51, 110, 2.0F, -18.1902F, -11.5119F, 3, 1, 1, 0.0F, false));

		bone45 = new RendererModel(this);
		bone45.setRotationPoint(13.25F, -12.6626F, -30.3389F);
		rotation6.addChild(bone45);
		setRotationAngle(bone45, -0.4363F, 0.0F, 0.0F);
		

		landtype_translate_z = new RendererModel(this);
		landtype_translate_z.setRotationPoint(0.0F, 0.8966F, -4.3813F);
		rotation6.addChild(landtype_translate_z);
		landtype_translate_z.cubeList.add(new ModelBox(landtype_translate_z, 13, 121, -4.5F, -19.0868F, -14.1305F, 2, 1, 1, 0.0F, false));

		bone105 = new RendererModel(this);
		bone105.setRotationPoint(0.0F, 0.8966F, -7.3813F);
		rotation6.addChild(bone105);
		bone105.cubeList.add(new ModelBox(bone105, 13, 121, -1.5F, -19.0868F, -14.1305F, 2, 1, 1, 0.0F, false));

		increment_translate_z = new RendererModel(this);
		increment_translate_z.setRotationPoint(0.0F, 0.8966F, -4.3813F);
		rotation6.addChild(increment_translate_z);
		increment_translate_z.cubeList.add(new ModelBox(increment_translate_z, 18, 124, 1.5F, -19.0868F, -14.1305F, 2, 1, 1, 0.0F, false));

		communicator = new RendererModel(this);
		communicator.setRotationPoint(0.0F, 0.8966F, -4.3813F);
		rotation6.addChild(communicator);
		communicator.cubeList.add(new ModelBox(communicator, 5, 105, 3.0F, -19.7368F, -27.1305F, 2, 1, 8, 0.0F, false));
		communicator.cubeList.add(new ModelBox(communicator, 0, 106, 2.5F, -18.9868F, -27.6305F, 3, 1, 3, 0.0F, false));
		communicator.cubeList.add(new ModelBox(communicator, 0, 106, 2.5F, -18.9868F, -21.6305F, 3, 1, 3, 0.0F, false));

		North = new RendererModel(this);
		North.setRotationPoint(0.0F, 3.0F, 0.0F);
		

		rotation2 = new RendererModel(this);
		rotation2.setRotationPoint(0.0F, 0.0F, 0.0F);
		North.addChild(rotation2);
		setRotationAngle(rotation2, 0.3491F, 0.0F, 0.0F);
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 9, -3.75F, -18.2368F, -32.8805F, 3, 1, 3, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 9, -8.0F, -18.2368F, -32.8805F, 3, 1, 3, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 11, 0, -11.375F, -18.7368F, -29.8805F, 2, 1, 4, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 11, 0, 9.375F, -18.7368F, -29.8805F, 2, 1, 4, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 23, 9, -7.0F, -17.7368F, -34.3805F, 1, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 28, 9, -2.75F, -17.7368F, -34.3805F, 1, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 23, 9, 1.25F, -17.7368F, -34.3805F, 1, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 23, 9, 5.75F, -17.7368F, -34.3805F, 1, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 13, 9, 0.25F, -18.2368F, -32.8805F, 3, 1, 3, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 13, 9, 4.5F, -18.2368F, -32.8805F, 3, 1, 3, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 34, 15, 12.25F, -16.7015F, -30.1418F, 2, 1, 2, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 12, 19, 12.5F, -17.2015F, -33.2668F, 2, 1, 2, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 5, 21, 15.25F, -17.2015F, -32.7668F, 1, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 33, 11, -13.75F, -18.2015F, -32.5168F, 1, 2, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 26, 13, -14.25F, -17.0765F, -33.0168F, 2, 1, 2, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 26, 13, -16.75F, -17.0765F, -33.0168F, 2, 1, 2, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 34, 7, -14.5F, -17.4515F, -29.7668F, 1, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 26, 13, -15.0F, -16.9515F, -30.2668F, 2, 1, 2, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 33, 11, -16.25F, -18.2015F, -32.5168F, 1, 2, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 20, 0, -7.25F, -18.0902F, -14.5119F, 4, 1, 2, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 24, 16, -12.75F, -18.4652F, -25.0119F, 3, 1, 3, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 20, 0, 3.25F, -18.0902F, -14.5119F, 4, 1, 2, 0.0F, false));

		bone96 = new RendererModel(this);
		bone96.setRotationPoint(0.0F, 0.0F, 0.0F);
		rotation2.addChild(bone96);
		bone96.cubeList.add(new ModelBox(bone96, 0, 0, -10.875F, -19.2368F, -33.8805F, 1, 1, 7, 0.0F, false));

		Door_rotate_x = new RendererModel(this);
		Door_rotate_x.setRotationPoint(10.375F, -18.2368F, -26.6305F);
		rotation2.addChild(Door_rotate_x);
		Door_rotate_x.cubeList.add(new ModelBox(Door_rotate_x, 0, 0, -0.5F, -1.0F, -7.25F, 1, 1, 7, 0.0F, false));

		bone51 = new RendererModel(this);
		bone51.setRotationPoint(13.25F, -12.6626F, -30.3389F);
		rotation2.addChild(bone51);
		setRotationAngle(bone51, -0.4363F, 0.0F, 0.0F);
		bone51.cubeList.add(new ModelBox(bone51, 33, 3, -0.5F, -5.9867F, -0.805F, 1, 2, 1, 0.0F, false));

		sonicport = new RendererModel(this);
		sonicport.setRotationPoint(0.0F, 0.8966F, -4.3813F);
		rotation2.addChild(sonicport);
		sonicport.cubeList.add(new ModelBox(sonicport, 21, 4, -1.5F, -19.2368F, -10.6305F, 3, 1, 3, 0.0F, false));

		bone60 = new RendererModel(this);
		bone60.setRotationPoint(-4.25F, -13.5513F, -20.9589F);
		rotation2.addChild(bone60);
		setRotationAngle(bone60, -0.2618F, 0.0F, 0.0F);
		bone60.cubeList.add(new ModelBox(bone60, 8, 15, -3.0F, -2.9867F, 1.555F, 2, 1, 2, 0.0F, false));
		bone60.cubeList.add(new ModelBox(bone60, 8, 15, 5.0F, -2.9867F, 1.555F, 2, 1, 2, 0.0F, false));
		bone60.cubeList.add(new ModelBox(bone60, 8, 15, 9.0F, -2.9867F, 1.555F, 2, 1, 2, 0.0F, false));
		bone60.cubeList.add(new ModelBox(bone60, 8, 15, 1.0F, -2.9867F, 1.555F, 2, 1, 2, 0.0F, false));
		bone60.cubeList.add(new ModelBox(bone60, 18, 15, 3.0F, -3.2367F, -5.445F, 2, 1, 2, 0.0F, false));
		bone60.cubeList.add(new ModelBox(bone60, 15, 14, 5.5F, -2.9867F, -4.945F, 1, 1, 1, 0.0F, false));
		bone60.cubeList.add(new ModelBox(bone60, 15, 14, 9.5F, -2.9867F, -4.945F, 1, 1, 1, 0.0F, false));
		bone60.cubeList.add(new ModelBox(bone60, 15, 14, -2.5F, -2.9867F, -4.945F, 1, 1, 1, 0.0F, false));
		bone60.cubeList.add(new ModelBox(bone60, 15, 14, 1.5F, -2.9867F, -4.945F, 1, 1, 1, 0.0F, false));

		bone57 = new RendererModel(this);
		bone57.setRotationPoint(-6.0F, -12.4222F, -18.6272F);
		rotation2.addChild(bone57);
		setRotationAngle(bone57, -0.1745F, 0.0F, 0.0F);
		bone57.cubeList.add(new ModelBox(bone57, 0, 14, -0.75F, -4.4468F, -4.1422F, 1, 1, 5, 0.0F, false));

		bone58 = new RendererModel(this);
		bone58.setRotationPoint(-3.0F, -12.4222F, -18.6272F);
		rotation2.addChild(bone58);
		setRotationAngle(bone58, -0.1745F, 0.0F, 0.0F);
		bone58.cubeList.add(new ModelBox(bone58, 0, 14, 0.25F, -4.4468F, -4.1422F, 1, 1, 5, 0.0F, false));

		bone61 = new RendererModel(this);
		bone61.setRotationPoint(3.0F, -12.4222F, -18.6272F);
		rotation2.addChild(bone61);
		setRotationAngle(bone61, -0.1745F, 0.0F, 0.0F);
		bone61.cubeList.add(new ModelBox(bone61, 0, 14, -1.75F, -4.4468F, -4.1422F, 1, 1, 5, 0.0F, false));

		bone62 = new RendererModel(this);
		bone62.setRotationPoint(6.0F, -12.4222F, -18.6272F);
		rotation2.addChild(bone62);
		setRotationAngle(bone62, -0.1745F, 0.0F, 0.0F);
		bone62.cubeList.add(new ModelBox(bone62, 0, 14, -0.75F, -4.4468F, -4.1422F, 1, 1, 5, 0.0F, false));

		rotor = new RendererModel(this);
		rotor.setRotationPoint(0.0F, -10.0F, 0.0F);
		rotor.cubeList.add(new ModelBox(rotor, 116, 109, -1.5F, -17.5F, -1.6F, 3, 14, 3, 0.0F, false));
		rotor.cubeList.add(new ModelBox(rotor, 116, 109, -1.5F, -55.5F, -1.6F, 3, 14, 3, 0.0F, false));

		top_translate_y = new RendererModel(this);
		top_translate_y.setRotationPoint(0.0F, 0.0F, 6.0F);
		rotor.addChild(top_translate_y);
		top_translate_y.cubeList.add(new ModelBox(top_translate_y, 107, 110, -1.0F, -45.5F, -7.1F, 2, 14, 2, 0.0F, false));

		bottom_translate_y = new RendererModel(this);
		bottom_translate_y.setRotationPoint(-0.5F, -14.0F, 6.5F);
		rotor.addChild(bottom_translate_y);
		bottom_translate_y.cubeList.add(new ModelBox(bottom_translate_y, 107, 110, -0.5F, -13.5F, -7.6F, 2, 14, 2, 0.0F, false));
	}

	public void render(ConsoleTile tile) {
		
		 
		
		borders.render(0.0625F);
		edges2.render(0.0625F);
		edges.render(0.0625F);
		panels2.render(0.0625F);
		panels.render(0.0625F);
		topedges.render(0.0625F);
		centre.render(0.0625F);
		base.render(0.0625F);
		NorthWest.render(0.0625F);
		NorthEast.render(0.0625F);
		South.render(0.0625F);
		SouthWest.render(0.0625F);
		North.render(0.0625F);
		rotor.render(0.0625F);
		
		ModelHelper.renderPartBrightness(1.0F, glow);
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}
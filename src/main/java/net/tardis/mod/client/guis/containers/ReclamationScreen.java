package net.tardis.mod.client.guis.containers;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.containers.ReclamationContainer;

public class ReclamationScreen extends ContainerScreen<ReclamationContainer>{
	
	private static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/containers/generic_54_container.png"); 
	
	public ReclamationScreen(ReclamationContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
		super(screenContainer, inv, titleIn);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		this.renderBackground();
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		int guiW = 176, guiH = 222;
		this.blit(width / 2 - guiW / 2, height / 2 - guiH / 2, 0, 0, guiW, guiH);
	}
	
	@Override
	public void render(int mouseX, int mouseY, float partialTicks) {
		super.render(mouseX, mouseY, partialTicks);
		this.renderHoveredToolTip(mouseX, mouseY);
	}
}

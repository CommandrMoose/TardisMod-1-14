package net.tardis.mod.client.guis;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.ChangePageButton;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ARSPiece;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.UpdateARSTablet;
import net.tardis.mod.registries.TardisRegistries;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ArsTabletScreen extends Screen {
	
	private static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/ars_tablet.png");

	public static int WIDTH = 410, HEIGHT = 235;
	public int page = 0;
	
	public ArsTabletScreen() {
		super(new StringTextComponent(""));
	}
	
	public ArsTabletScreen(GuiContext cont) {
		this();
	}

	@Override
	protected void init() {
		super.init();
		this.loadItems();
	}

	public void loadItems(){

		for(Widget w : this.buttons) {
			w.active = false;
		}
		buttons.clear();

		// Takes the number of ARS rooms and adds them to a button list.

		int x = width / 2 - 100, y = height / 2 + 50;
		List<ARSPiece> arsPiecesList = new ArrayList<ARSPiece>();
		for(ARSPiece p : TardisRegistries.ARS_PIECES.getRegistry().values()) {
			arsPiecesList.add(p);
		}

		int maxPages = (int)Math.ceil(arsPiecesList.size() / 7);
		if(arsPiecesList.size() > page * 7) {
			arsPiecesList = arsPiecesList.subList(page * 7, arsPiecesList.size());
		}

        this.addButtonToScreen(arsPiecesList, x, y, 7);

		this.addButton(new ChangePageButton(WIDTH, HEIGHT, true, button -> {
			page = MathHelper.clamp(page + 1, 0, maxPages);
			this.loadItems();
		}, false));

		this.addButton(new ChangePageButton(WIDTH - 20, HEIGHT, false, button -> {
			page = MathHelper.clamp(page - 1, 0, maxPages);
			this.loadItems();
		}, false));
	}

	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		int width = 256, height = 173;
		this.blit(this.width / 2 - width / 2, this.height / 2 - height / 2, 0, 0, width, height);

		super.render(p_render_1_, p_render_2_, p_render_3_);
		this.drawCenteredString(this.font, new TranslationTextComponent("ars.message.gui_title").getFormattedText(), this.width / 2, this.height / 2 - (height / 2 - 30), 0xFFFFFF);
		this.drawCenteredString(this.font, new TranslationTextComponent("ars.message.gui_info").getFormattedText(), this.width / 2, this.height / 2 - (height / 2 - 50), 0xFFFFFF);
	}

    public List<ARSPiece> addButtonToScreen(List<ARSPiece> list, int startX, int startY, int max) {
		int index = 0;

		if (max == 0)
			return list;

		if (max > list.size())
			max = list.size();

		Iterator<ARSPiece> it = list.iterator();
		while (it.hasNext()) {

			ARSPiece ars = it.next();
			this.addButton(new TextButton(startX, startY - (index * this.font.FONT_HEIGHT) + 5, ars.getTranslation().getFormattedText(), but -> {
				Network.INSTANCE.sendToServer(new UpdateARSTablet(ars.getRegistryName()));
				Minecraft.getInstance().displayGuiScreen(null);
			}));
			it.remove();

			++index;
			if (index >= max)
				return list;
		}
		return list;
	}
}

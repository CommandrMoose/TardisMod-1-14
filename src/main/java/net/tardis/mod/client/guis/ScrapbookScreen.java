package net.tardis.mod.client.guis;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.ChangePageButton;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.widgets.Page;
import net.tardis.mod.contexts.gui.GuiItemContext;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.UpdateManualPageMessage;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ScrapbookScreen extends Screen {

	// Yes I know its a copy/paste. Will come in and properly extend items soon.

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/scrapbook.png");
	public static final int WIDTH = 256, HEIGHT = 187;
	List<Page> pages = new ArrayList<Page>();
	int page = 0;

	public ScrapbookScreen() {
		super(new StringTextComponent("Scrapbook"));
	}

	public ScrapbookScreen(GuiContext contex) {
		super(new StringTextComponent("Scrapbook"));
		if(contex instanceof GuiItemContext) {
			ItemStack stack = ((GuiItemContext)contex).getItemStack();
			if(stack.hasTag() && stack.getTag().contains("page"))
				page = stack.getTag().getInt("page");
				
		}
	}

	@Override
	protected void init() {
		super.init();
		
		for(Page p : PageReader.readPages())
			this.pages.add(p);

		this.buttons.clear();
		
		this.addButton(new ChangePageButton(width / 2 + 85, height / 2 + 45, true, button -> {
			if(page + 2 < pages.size())
				page += 2;
		}, true));
		this.addButton(new ChangePageButton(width / 2 - 110, height / 2 + 45, false, button -> {
			if(page - 2 >= 0) {
				page -= 2;
			}
		}, true));
	}
	
	public void addPage(String... lines) {
		this.pages.add(new Page(this.font, lines));
	}

	@Override
	public void render(int mouseX, int mouseY, float p_render_3_) {
		this.renderBackground();
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		this.blit(width / 2 - WIDTH / 2, height / 2 - HEIGHT / 2, 0, 0, WIDTH, HEIGHT);
		if(pages.size() > page) {
			this.pages.get(page).render(width / 2 - 110, height / 2 - 70, mouseX, mouseY);
		}
		if(page + 1 < this.pages.size()) {
			this.pages.get(page + 1).render(width / 2 + 10, height / 2 - 70, mouseX, mouseY);
		}
		
		for(Widget w : this.buttons) {
			w.render(mouseX, mouseY, p_render_3_);
		}
		
		//Page numbers
		//this.font.drawString("" + (page + 1), width / 2 - 60, height / 2 + 50, 0x000000);
		//this.font.drawString("" + (page + 2), width / 2 + 50, height / 2 + 50, 0x000000);
	}

	@Override
	public void onClose() {
		super.onClose();
		Network.sendToServer(new UpdateManualPageMessage(page));
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int p_mouseClicked_5_) {
		
		return super.mouseClicked(mouseX, mouseY, p_mouseClicked_5_);
	}
	
	public static class PageReader{
		
		public static final ResourceLocation INDEX = new ResourceLocation(Tardis.MODID, "scrapbook/index.json");
		
		public static List<Page> readPages(){
			List<Page> pages = new ArrayList<Page>();
			try {
				for(ResourceLocation loc : readIndex()) {
					JsonObject doc = new JsonParser().parse(new InputStreamReader(Minecraft.getInstance().getResourceManager().getResource(loc).getInputStream())).getAsJsonObject();
					Page page = new Page(Minecraft.getInstance().fontRenderer);
					for(JsonElement line : doc.get("lines").getAsJsonArray()) {
						page.addLineWrapped(line.getAsString());
					}
					pages.add(page);
				}
			}
			catch(IOException e) {
				e.printStackTrace();
			}
			return pages;
		}
		
		private static List<ResourceLocation> readIndex(){
			List<ResourceLocation> pages = new ArrayList<ResourceLocation>();
			try {
				JsonObject doc = new JsonParser().parse(new InputStreamReader(Minecraft.getInstance().getResourceManager().getResource(INDEX).getInputStream())).getAsJsonObject();
				JsonArray vals = doc.get("pages").getAsJsonArray();
				for(JsonElement s : vals) {
					pages.add(new ResourceLocation(s.getAsString() + ".json"));
//					System.out.println("TARDIS Debug: Read manual page: " + s.getAsString() + ".json");
				}
			}
			catch(IOException e) {
				e.printStackTrace();
			}
			return pages;
		}
	}
}

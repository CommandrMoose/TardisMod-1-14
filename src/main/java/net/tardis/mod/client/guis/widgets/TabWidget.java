package net.tardis.mod.client.guis.widgets;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;

public class TabWidget extends Button {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/widgets/tabs.png");
	public boolean isSelected;
	private ItemStack item;
	
	public TabWidget(int xIn, int yIn, ItemStack stack, IPressable press) {
		super(xIn, yIn, 35, 31, "", press);
		this.item = stack;
	}

	@Override
	public void renderButton(int p_render_1_, int p_render_2_, float p_render_3_) {
		GlStateManager.pushMatrix();
		GlStateManager.enableAlphaTest();
		GlStateManager.enableBlend();
		RenderHelper.enableGUIStandardItemLighting();
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		if(this.isSelected)
			blit(x, y, 0, 34, 34, 66);
		else blit(x, y, 0, 0, 43, 31);
		Minecraft.getInstance().getItemRenderer().renderItemAndEffectIntoGUI(item, x + 8, y + 8);
		GlStateManager.disableAlphaTest();
		RenderHelper.enableStandardItemLighting();
		GlStateManager.disableBlend();
		GlStateManager.popMatrix();
	}

}
package net.tardis.mod.schematics;

import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.registries.TardisRegistries;

public class Schematics {
	
	public static ExteriorUnlockSchematic POLICE_BOX;
	
	public static void registerAll() {
		TardisRegistries.SCHEMATICS.register("police_box", POLICE_BOX = new ExteriorUnlockSchematic(() -> ExteriorRegistry.POLICE_BOX));
	}

}

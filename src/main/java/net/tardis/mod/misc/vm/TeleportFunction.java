package net.tardis.mod.misc.vm;


import java.util.Map;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.Helper;



public class TeleportFunction extends ParentFunction{
	
	public TeleportFunction(Map<Integer, IVortexMFunction> subFunctions) {
		super(subFunctions);
	}
	
	public TeleportFunction() {
		
	}

	@Override
	public void onActivated(World world, PlayerEntity player){
		if (world.isRemote && Helper.canVMTravelToDimension(world.getDimension().getType())) {
				Tardis.proxy.openGUI(Constants.Gui.VORTEX_TELE, null);
				super.onActivated(world, player);
		}
		else {
			player.sendStatusMessage(new TranslationTextComponent("message.vm.forbidden"), false);	
		}
	}

	@Override
	public String getNameKey() {
		return new TranslationTextComponent("function.vm.teleport").getFormattedText();
	}

	@Override
	public Boolean stateComplete() {
		return true;
	}

	@Override
	public void sendActionToServer(World world, ServerPlayerEntity player) {
		
	}
}

package net.tardis.mod.misc.vm;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;
import net.tardis.mod.sounds.TSounds;

/**
 * VM SubFunction
 * @implSpec Extend this class, and use same parameters for first constructor
 * @implNote Ensure to call super
 */
public abstract class SubFunction implements IVortexMFunction{
	
	@SuppressWarnings("unused")
	private final ParentFunction parent;
	
	public SubFunction (ParentFunction parent) {
		this.parent = parent;
	}
	
	@Override
	public void onActivated(World world, PlayerEntity player) {
		world.playSound(player, player.getPosition(), TSounds.VM_BUTTON, SoundCategory.PLAYERS, 0.5F, 1F);
	}
	
	@Override
	public void sendActionToServer(World world, ServerPlayerEntity player) {
		world.playSound(null, player.getPosition(), TSounds.VM_BUTTON, SoundCategory.PLAYERS, 0.5F, 1F);
	}
	
	
	
	@Override
	public String getNameKey() {
		return "sub_function";
	}

	@Override
	public Boolean stateComplete() {
		return true;
	}

	@Override
	public Boolean isServerSide() {
		return false;
	}

}

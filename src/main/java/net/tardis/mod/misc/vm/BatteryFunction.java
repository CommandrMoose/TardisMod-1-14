package net.tardis.mod.misc.vm;

import java.util.Map;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.containers.VMContainer;
import net.tardis.mod.items.VortexManipItem;

public class BatteryFunction extends ParentFunction{

	/**
	 */
	public BatteryFunction(Map<Integer, IVortexMFunction> subFunctions) {
		super(subFunctions);
	}
	
	public BatteryFunction() {
	}

	/*TODO:
	 * Will display amount of charge left
	 * Will take more charge to jump longer distances
	 * Hook into Tardis Fuel Handler and refuel VM from Tardis engine via some sort of charger block/item
	 *  
	 */
	
	
	@Override
	public void onActivated(World world, PlayerEntity player){

	}
	
	@Override
	public void sendActionToServer(World world, ServerPlayerEntity player) {
		ItemStack currentItem = player.inventory.getCurrentItem();
		if (currentItem.getItem() instanceof VortexManipItem) {
			currentItem.getCapability(Capabilities.VORTEX_MANIP).ifPresent((cap)-> {
				NetworkHooks.openGui((ServerPlayerEntity)player,new INamedContainerProvider() {
					@Override
					public Container createMenu(int id, PlayerInventory inv, PlayerEntity player) {
						return new VMContainer(id, inv, currentItem);
					}

					@Override
					public ITextComponent getDisplayName() {
						return new TranslationTextComponent("vm.inventory.battery");
					}
					}, 
						buf -> buf.writeItemStack(currentItem)
						);
				super.onActivated(world, player);
				});
		}
	}

	@Override
	public String getNameKey() {
		return new TranslationTextComponent("function.vm.battery").getFormattedText();
	}

	@Override
	public Boolean stateComplete() {
		return true;
	}

	@Override
	public Boolean isServerSide() {
		return true;
	}
	
	

}

package net.tardis.mod.misc;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;

public class ContainerProvider<T extends Container> implements INamedContainerProvider{

	private TranslationTextComponent trans;
	private IContainerCreator<T> creator;
	
	public ContainerProvider(String key, IContainerCreator<T> create) {
		this.trans = new TranslationTextComponent(key);
		this.creator = create;
	}
	
	public ContainerProvider(IContainerCreator<T> create) {
		this("", create);
	}
	
	@Override
	public Container createMenu(int id, PlayerInventory playerInv, PlayerEntity player) {
		return creator.create(id, playerInv, player);
	}

	@Override
	public ITextComponent getDisplayName() {
		return this.trans;
	}

	@FunctionalInterface
	public static interface IContainerCreator<T extends Container>{
		
		T create(int id, PlayerInventory inv, PlayerEntity player);
		
	}
}

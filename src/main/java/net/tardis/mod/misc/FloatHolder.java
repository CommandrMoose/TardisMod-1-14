package net.tardis.mod.misc;

public class FloatHolder {
	
	float val;
	
	public void add(float val) {
		this.val += val;
	}
	
	public void set(float val) {
		this.val = val;
	}
	
	public float getValue() {
		return this.val;
	}

}

package net.tardis.mod.datagen;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;

import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.util.Direction;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.Helper;

public class BlockstateTagGen {

	private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	
	public BlockstateTagGen(){}
	
	public void create(DataGenerator gen) {
		
		for(Block block : ForgeRegistries.BLOCKS) {
			if(block.getRegistryName().getNamespace().contentEquals(Tardis.MODID)) {
				File blockstate = gen.getOutputFolder().resolve("assets/" + Tardis.MODID +
						"/blockstates/" + block.getRegistryName().getPath() + ".json").toFile();
				if(!blockstate.exists()) {
					try {
						blockstate.getParentFile().mkdirs();
						blockstate.createNewFile();
						
						JsonWriter writer = GSON.newJsonWriter(new FileWriter(blockstate));
						writer.beginObject();
						
						writer.name("forge_marker").value(1);
						writer.name("variants");
						writer.beginObject();
						if(block.getStateContainer().getProperty("facing") != null) {
							writer.name("facing");
							writer.beginObject();
							for(Object obj : block.getStateContainer().getProperty("facing").getAllowedValues()) {
								Direction dir = (Direction)obj;
								writer.name(dir.getName());
								writer.beginObject();
								writer.name("y").value((int)Helper.getAngleFromFacing(dir));
								writer.endObject();
							}
							writer.endObject();
						}
						else {
							writer.name("");
							writer.beginObject();
							writer.endObject();
						}
						writer.endObject();
						
						writer.endObject();
						writer.close();
					}
					catch(IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	} 
}

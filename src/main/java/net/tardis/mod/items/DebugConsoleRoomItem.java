package net.tardis.mod.items;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler;
import org.apache.logging.log4j.core.jmx.Server;

import java.text.DecimalFormat;
import java.util.List;


/**
 * A debug tool for users to easily generate console rooms.
 * 
 */
public class DebugConsoleRoomItem extends Item{

	ResourceLocation file = new ResourceLocation(Tardis.MODID, "tardis/structures/debug_console_room");

	public DebugConsoleRoomItem() {
		super(new Item.Properties().maxStackSize(64));
	}

	@Override
	public ActionResultType onItemUse(ItemUseContext context) {

		if (!context.getWorld().isRemote) {
			ServerWorld worldIn = (ServerWorld) context.getWorld();

			BlockPos offset = new BlockPos(30F,1F,30F);
			Template temp = worldIn.getStructureTemplateManager().getTemplate(file);
			context.getWorld().getServer().enqueue(new TickDelayedTask(1, () -> {
				temp.addBlocksToWorld(worldIn, context.getPos().subtract(offset), new PlacementSettings().setIgnoreEntities(false));
			}));


		}

		return super.onItemUse(context);
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		tooltip.add(new TranslationTextComponent("tooltip." + Tardis.MODID + ".debug_console"));
		super.addInformation(stack, worldIn, tooltip, flagIn);
	}

}

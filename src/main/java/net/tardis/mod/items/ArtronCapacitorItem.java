package net.tardis.mod.items;

import java.util.List;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.properties.Prop;

public class ArtronCapacitorItem extends Item {

	private float storage = 32F;
	private float rechargeModifier = 1F;
	
	public ArtronCapacitorItem(float storage, float rechargeMod) {
		super(Prop.Items.ONE.get().group(TItemGroups.MAINTENANCE));
		this.storage = storage;
		this.rechargeModifier = rechargeMod;
	}
	
	public float getRechangeModifier() {
		return this.rechargeModifier;
	}
	
	public float getMaxStorage() {
		return this.storage;
	}
	
	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslationTextComponent("tooltip.artron_capacitor.max_charge", this.storage));
		tooltip.add(new TranslationTextComponent("tooltip.artron_capacitor.recharge_multiplier", this.rechargeModifier));
		tooltip.add(new TranslationTextComponent("item.info.shift"));
		if (Screen.hasShiftDown()) {
			tooltip.clear();
			tooltip.add(0, this.getDisplayName(stack));
			tooltip.add(new TranslationTextComponent("tooltip.artron_capacitor.info"));
			tooltip.add(new TranslationTextComponent("tooltip.artron_capacitor.howto"));
		}
	}

}

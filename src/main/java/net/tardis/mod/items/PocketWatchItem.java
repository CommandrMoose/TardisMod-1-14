package net.tardis.mod.items;

import java.util.List;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ItemFrameEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.IWatch;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.sounds.TSounds;

public class PocketWatchItem extends Item {

	public PocketWatchItem() {
		super(Prop.Items.ONE.get().group(TItemGroups.MAIN));
		this.addPropertyOverride(new ResourceLocation(Tardis.MODID, "rot"), (stack, world, entity) -> {
			IWatch watch = stack.getCapability(Capabilities.WATCH_CAPABILITY).orElse(null);
			if(watch != null) {
				if(entity != null && watch.shouldSpin(entity))
					return entity.ticksExisted % 8;
			}
			return world != null ? getRotFromTime(world) : 0;
		});
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		tooltip.add(new TranslationTextComponent("tooltip." + Tardis.MODID + ".watch.line1"));
		tooltip.add(new TranslationTextComponent("tooltip." + Tardis.MODID + ".watch.line2"));
		super.addInformation(stack, worldIn, tooltip, flagIn);
	}

	@Override
	public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
		if (entityIn instanceof PlayerEntity || entityIn instanceof ItemFrameEntity) {
			if(PlayerHelper.isInEitherHand((LivingEntity) entityIn, stack.getItem()) || entityIn instanceof ItemFrameEntity) {
				if (worldIn.getGameTime() % 20 == 0) {
					worldIn.playSound(null, entityIn.getPosition(), TSounds.WATCH_TICK, SoundCategory.BLOCKS, 0.25F, 1F);
				}
				if(worldIn.getGameTime() % 100 == 0) {
					stack.getCapability(Capabilities.WATCH_CAPABILITY).ifPresent(watch -> 
					{
						watch.tick(worldIn, entityIn);
						if (watch.shouldSpin(entityIn) && worldIn.getGameTime() % 10 == 0) {
							worldIn.playSound(null, entityIn.getPosition(), TSounds.WATCH_MALFUNCTION, SoundCategory.BLOCKS, 0.5F, 1F);
						}
					});	
				}
			}
		}
	}
	
	@OnlyIn(Dist.CLIENT)
	public float getRotFromTime(World world){
		//float per = world % 12000.0F / 12000.0F;
		//float rot = per * 360.0F;
		float rot = ((world.getDimension().calculateCelestialAngle(world.getDayTime(), 0) * 2.0F) % 360.0F) * 360.0F;
		
		if(rot > 315)
			return 7;
		if(rot > 270)
			return 6;
		if(rot > 225)
			return 5;
		if(rot > 180)
			return 4;
		if(rot > 135)
			return 3;
		if(rot > 90)
			return 2;
		if(rot > 45)
			return 1;
		
		return 0F;
	}

}
